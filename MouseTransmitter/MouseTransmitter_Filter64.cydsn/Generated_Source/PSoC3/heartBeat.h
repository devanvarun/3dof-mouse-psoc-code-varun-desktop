/*******************************************************************************
* File Name: heartBeat.h  
* Version 3.0
*
* Description:
*  Contains the prototypes and constants for the functions available to the 
*  PWM user module.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PWM_heartBeat_H)
#define CY_PWM_heartBeat_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 heartBeat_initVar;


/***************************************
* Conditional Compilation Parameters
***************************************/
#define heartBeat_Resolution 8u
#define heartBeat_UsingFixedFunction 0u
#define heartBeat_DeadBandMode 0u
#define heartBeat_KillModeMinTime 0u
#define heartBeat_KillMode 0u
#define heartBeat_PWMMode 0u
#define heartBeat_PWMModeIsCenterAligned 0u
#define heartBeat_DeadBandUsed 0u
#define heartBeat_DeadBand2_4 0u

#if !defined(heartBeat_PWMUDB_genblk8_stsreg__REMOVED)
    #define heartBeat_UseStatus 1u
#else
    #define heartBeat_UseStatus 0u
#endif /* !defined(heartBeat_PWMUDB_genblk8_stsreg__REMOVED) */

#if !defined(heartBeat_PWMUDB_genblk1_ctrlreg__REMOVED)
    #define heartBeat_UseControl 1u
#else
    #define heartBeat_UseControl 0u
#endif /* !defined(heartBeat_PWMUDB_genblk1_ctrlreg__REMOVED) */
#define heartBeat_UseOneCompareMode 1u
#define heartBeat_MinimumKillTime 1u
#define heartBeat_EnableMode 0u

#define heartBeat_CompareMode1SW 0u
#define heartBeat_CompareMode2SW 0u

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component PWM_v3_0 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Use Kill Mode Enumerated Types */
#define heartBeat__B_PWM__DISABLED 0
#define heartBeat__B_PWM__ASYNCHRONOUS 1
#define heartBeat__B_PWM__SINGLECYCLE 2
#define heartBeat__B_PWM__LATCHED 3
#define heartBeat__B_PWM__MINTIME 4


/* Use Dead Band Mode Enumerated Types */
#define heartBeat__B_PWM__DBMDISABLED 0
#define heartBeat__B_PWM__DBM_2_4_CLOCKS 1
#define heartBeat__B_PWM__DBM_256_CLOCKS 2


/* Used PWM Mode Enumerated Types */
#define heartBeat__B_PWM__ONE_OUTPUT 0
#define heartBeat__B_PWM__TWO_OUTPUTS 1
#define heartBeat__B_PWM__DUAL_EDGE 2
#define heartBeat__B_PWM__CENTER_ALIGN 3
#define heartBeat__B_PWM__DITHER 5
#define heartBeat__B_PWM__HARDWARESELECT 4


/* Used PWM Compare Mode Enumerated Types */
#define heartBeat__B_PWM__LESS_THAN 1
#define heartBeat__B_PWM__LESS_THAN_OR_EQUAL 2
#define heartBeat__B_PWM__GREATER_THAN 3
#define heartBeat__B_PWM__GREATER_THAN_OR_EQUAL_TO 4
#define heartBeat__B_PWM__EQUAL 0
#define heartBeat__B_PWM__FIRMWARE 5



/***************************************
* Data Struct Definition
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for PWM Component
 *************************************************************************/
typedef struct
{
    
    uint8 PWMEnableState;
       
    #if(!heartBeat_UsingFixedFunction)
        uint8 PWMUdb;               /* PWM Current Counter value  */
        #if(!heartBeat_PWMModeIsCenterAligned)
            uint8 PWMPeriod;
        #endif /* (!heartBeat_PWMModeIsCenterAligned) */
        #if (heartBeat_UseStatus)
            uint8 InterruptMaskValue;   /* PWM Current Interrupt Mask */
        #endif /* (heartBeat_UseStatus) */
        
        /* Backup for Deadband parameters */
        #if(heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_256_CLOCKS || \
            heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_2_4_CLOCKS)
            uint8 PWMdeadBandValue; /* Dead Band Counter Current Value */
        #endif /* deadband count is either 2-4 clocks or 256 clocks */
        
        /* Backup Kill Mode Counter*/
        #if(heartBeat_KillModeMinTime)
            uint8 PWMKillCounterPeriod; /* Kill Mode period value */
        #endif /* (heartBeat_KillModeMinTime) */
       
        /* Backup control register */
        #if(heartBeat_UseControl)
            uint8 PWMControlRegister; /* PWM Control Register value */
        #endif /* (heartBeat_UseControl) */
        
    #endif /* (!heartBeat_UsingFixedFunction) */
   
}heartBeat_backupStruct;


/***************************************
*        Function Prototypes
 **************************************/
 
void    heartBeat_Start(void) ;
void    heartBeat_Stop(void) ;

#if (heartBeat_UseStatus || heartBeat_UsingFixedFunction)
    void  heartBeat_SetInterruptMode(uint8 interruptMode) ;
    uint8 heartBeat_ReadStatusRegister(void) ;
#endif /* (heartBeat_UseStatus || heartBeat_UsingFixedFunction) */

#define heartBeat_GetInterruptSource() heartBeat_ReadStatusRegister()

#if (heartBeat_UseControl)
    uint8 heartBeat_ReadControlRegister(void) ; 
    void  heartBeat_WriteControlRegister(uint8 control) ;
#endif /* (heartBeat_UseControl) */

#if (heartBeat_UseOneCompareMode)
   #if (heartBeat_CompareMode1SW)
       void    heartBeat_SetCompareMode(uint8 comparemode) ;
   #endif /* (heartBeat_CompareMode1SW) */
#else
    #if (heartBeat_CompareMode1SW)
        void    heartBeat_SetCompareMode1(uint8 comparemode) ;
    #endif /* (heartBeat_CompareMode1SW) */
    #if (heartBeat_CompareMode2SW)
        void    heartBeat_SetCompareMode2(uint8 comparemode) ;
    #endif /* (heartBeat_CompareMode2SW) */
#endif /* (heartBeat_UseOneCompareMode) */

#if (!heartBeat_UsingFixedFunction)
    uint8   heartBeat_ReadCounter(void) ;
    uint8 heartBeat_ReadCapture(void) ;
    
	#if (heartBeat_UseStatus)
	        void heartBeat_ClearFIFO(void) ;
	#endif /* (heartBeat_UseStatus) */

    void    heartBeat_WriteCounter(uint8 counter) ;
#endif /* (!heartBeat_UsingFixedFunction) */

void    heartBeat_WritePeriod(uint8 period) ;
uint8 heartBeat_ReadPeriod(void) ;

#if (heartBeat_UseOneCompareMode)
    void    heartBeat_WriteCompare(uint8 compare) ;
    uint8 heartBeat_ReadCompare(void) ; 
#else
    void    heartBeat_WriteCompare1(uint8 compare) ;
    uint8 heartBeat_ReadCompare1(void) ; 
    void    heartBeat_WriteCompare2(uint8 compare) ;
    uint8 heartBeat_ReadCompare2(void) ; 
#endif /* (heartBeat_UseOneCompareMode) */


#if (heartBeat_DeadBandUsed)
    void    heartBeat_WriteDeadTime(uint8 deadtime) ;
    uint8   heartBeat_ReadDeadTime(void) ;
#endif /* (heartBeat_DeadBandUsed) */

#if ( heartBeat_KillModeMinTime)
    void heartBeat_WriteKillTime(uint8 killtime) ;
    uint8 heartBeat_ReadKillTime(void) ; 
#endif /* ( heartBeat_KillModeMinTime) */

void heartBeat_Init(void) ;
void heartBeat_Enable(void) ;
void heartBeat_Sleep(void) ;
void heartBeat_Wakeup(void) ;
void heartBeat_SaveConfig(void) ;
void heartBeat_RestoreConfig(void) ;


/***************************************
*         Initialization Values
**************************************/
#define heartBeat_INIT_PERIOD_VALUE        50u
#define heartBeat_INIT_COMPARE_VALUE1      25u
#define heartBeat_INIT_COMPARE_VALUE2      63u
#define heartBeat_INIT_INTERRUPTS_MODE     (uint8)(((uint8)(0u << heartBeat_STATUS_TC_INT_EN_MASK_SHIFT)) | \
                                                  (uint8)((uint8)(0u << heartBeat_STATUS_CMP2_INT_EN_MASK_SHIFT)) | \
                                                  (uint8)((uint8)(0u << heartBeat_STATUS_CMP1_INT_EN_MASK_SHIFT )) | \
                                                  (uint8)((uint8)(0u << heartBeat_STATUS_KILL_INT_EN_MASK_SHIFT )))
#define heartBeat_DEFAULT_COMPARE2_MODE    (uint8)((uint8)1u << heartBeat_CTRL_CMPMODE2_SHIFT)
#define heartBeat_DEFAULT_COMPARE1_MODE    (uint8)((uint8)1u << heartBeat_CTRL_CMPMODE1_SHIFT)
#define heartBeat_INIT_DEAD_TIME           1u


/********************************
*         Registers
******************************** */

#if (heartBeat_UsingFixedFunction)
   #define heartBeat_PERIOD_LSB          (*(reg16 *) heartBeat_PWMHW__PER0)
   #define heartBeat_PERIOD_LSB_PTR      ( (reg16 *) heartBeat_PWMHW__PER0)
   #define heartBeat_COMPARE1_LSB        (*(reg16 *) heartBeat_PWMHW__CNT_CMP0)
   #define heartBeat_COMPARE1_LSB_PTR    ( (reg16 *) heartBeat_PWMHW__CNT_CMP0)
   #define heartBeat_COMPARE2_LSB        0x00u
   #define heartBeat_COMPARE2_LSB_PTR    0x00u
   #define heartBeat_COUNTER_LSB         (*(reg16 *) heartBeat_PWMHW__CNT_CMP0)
   #define heartBeat_COUNTER_LSB_PTR     ( (reg16 *) heartBeat_PWMHW__CNT_CMP0)
   #define heartBeat_CAPTURE_LSB         (*(reg16 *) heartBeat_PWMHW__CAP0)
   #define heartBeat_CAPTURE_LSB_PTR     ( (reg16 *) heartBeat_PWMHW__CAP0)
   #define heartBeat_RT1                 (*(reg8 *)  heartBeat_PWMHW__RT1)
   #define heartBeat_RT1_PTR             ( (reg8 *)  heartBeat_PWMHW__RT1)
      
#else
   #if (heartBeat_Resolution == 8u) /* 8bit - PWM */
	   
	   #if(heartBeat_PWMModeIsCenterAligned)
	       #define heartBeat_PERIOD_LSB      (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
	       #define heartBeat_PERIOD_LSB_PTR   ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
	   #else
	       #define heartBeat_PERIOD_LSB      (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F0_REG)
	       #define heartBeat_PERIOD_LSB_PTR   ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F0_REG)
	   #endif /* (heartBeat_PWMModeIsCenterAligned) */
	   
	   #define heartBeat_COMPARE1_LSB    (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D0_REG)
	   #define heartBeat_COMPARE1_LSB_PTR ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D0_REG)
	   #define heartBeat_COMPARE2_LSB    (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
	   #define heartBeat_COMPARE2_LSB_PTR ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
	   #define heartBeat_COUNTERCAP_LSB   (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A1_REG)
	   #define heartBeat_COUNTERCAP_LSB_PTR ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A1_REG)
	   #define heartBeat_COUNTER_LSB     (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A0_REG)
	   #define heartBeat_COUNTER_LSB_PTR  ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A0_REG)
	   #define heartBeat_CAPTURE_LSB     (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F1_REG)
	   #define heartBeat_CAPTURE_LSB_PTR  ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F1_REG)
   
   #else
   		#if(CY_PSOC3) /* 8-bit address space */	
			#if(heartBeat_PWMModeIsCenterAligned)
		       #define heartBeat_PERIOD_LSB      (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
		       #define heartBeat_PERIOD_LSB_PTR   ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
		    #else
		       #define heartBeat_PERIOD_LSB      (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F0_REG)
		       #define heartBeat_PERIOD_LSB_PTR   ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F0_REG)
		    #endif /* (heartBeat_PWMModeIsCenterAligned) */
		   
		    #define heartBeat_COMPARE1_LSB    (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D0_REG)
		    #define heartBeat_COMPARE1_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D0_REG)
		    #define heartBeat_COMPARE2_LSB    (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
		    #define heartBeat_COMPARE2_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__D1_REG)
		    #define heartBeat_COUNTERCAP_LSB   (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A1_REG)
		    #define heartBeat_COUNTERCAP_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A1_REG)
		    #define heartBeat_COUNTER_LSB     (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A0_REG)
		    #define heartBeat_COUNTER_LSB_PTR  ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__A0_REG)
		    #define heartBeat_CAPTURE_LSB     (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F1_REG)
		    #define heartBeat_CAPTURE_LSB_PTR  ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__F1_REG)
		#else
			#if(heartBeat_PWMModeIsCenterAligned)
		       #define heartBeat_PERIOD_LSB      (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
		       #define heartBeat_PERIOD_LSB_PTR   ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
		    #else
		       #define heartBeat_PERIOD_LSB      (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_F0_REG)
		       #define heartBeat_PERIOD_LSB_PTR   ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_F0_REG)
		    #endif /* (heartBeat_PWMModeIsCenterAligned) */
		   
		    #define heartBeat_COMPARE1_LSB    (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D0_REG)
		    #define heartBeat_COMPARE1_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D0_REG)
		    #define heartBeat_COMPARE2_LSB    (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
		    #define heartBeat_COMPARE2_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_D1_REG)
		    #define heartBeat_COUNTERCAP_LSB   (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_A1_REG)
		    #define heartBeat_COUNTERCAP_LSB_PTR ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_A1_REG)
		    #define heartBeat_COUNTER_LSB     (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_A0_REG)
		    #define heartBeat_COUNTER_LSB_PTR  ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_A0_REG)
		    #define heartBeat_CAPTURE_LSB     (*(reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_F1_REG)
		    #define heartBeat_CAPTURE_LSB_PTR  ((reg16 *) heartBeat_PWMUDB_sP8_pwmdp_u0__16BIT_F1_REG)
		#endif
		
	   #define heartBeat_AUX_CONTROLDP1    (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u1__DP_AUX_CTL_REG)
       #define heartBeat_AUX_CONTROLDP1_PTR  ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u1__DP_AUX_CTL_REG)

   #endif /* (heartBeat_Resolution == 8) */
   
   #define heartBeat_AUX_CONTROLDP0      (*(reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__DP_AUX_CTL_REG)
   #define heartBeat_AUX_CONTROLDP0_PTR  ((reg8 *) heartBeat_PWMUDB_sP8_pwmdp_u0__DP_AUX_CTL_REG)

#endif /* (heartBeat_UsingFixedFunction) */
   
#if(heartBeat_KillModeMinTime )
    #define heartBeat_KILLMODEMINTIME      (*(reg8 *) heartBeat_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    #define heartBeat_KILLMODEMINTIME_PTR   ((reg8 *) heartBeat_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    /* Fixed Function Block has no Kill Mode parameters because it is Asynchronous only */
#endif /* (heartBeat_KillModeMinTime ) */

#if(heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_256_CLOCKS)
    #define heartBeat_DEADBAND_COUNT      (*(reg8 *) heartBeat_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define heartBeat_DEADBAND_COUNT_PTR  ((reg8 *) heartBeat_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define heartBeat_DEADBAND_LSB_PTR    ((reg8 *) heartBeat_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
    #define heartBeat_DEADBAND_LSB        (*(reg8 *) heartBeat_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
#elif(heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_2_4_CLOCKS)
    /* In Fixed Function Block these bits are in the control blocks control register */
    #if (heartBeat_UsingFixedFunction)
        #define heartBeat_DEADBAND_COUNT        (*(reg8 *) heartBeat_PWMHW__CFG0) 
        #define heartBeat_DEADBAND_COUNT_PTR     ((reg8 *) heartBeat_PWMHW__CFG0)
        #define heartBeat_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << heartBeat_DEADBAND_COUNT_SHIFT)
        /* As defined by the Register Map as DEADBAND_PERIOD[1:0] in CFG0 */
        #define heartBeat_DEADBAND_COUNT_SHIFT   0x06u  
    #else
        /* Lower two bits of the added control register define the count 1-3 */
        #define heartBeat_DEADBAND_COUNT        (*(reg8 *) heartBeat_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define heartBeat_DEADBAND_COUNT_PTR     ((reg8 *) heartBeat_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define heartBeat_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << heartBeat_DEADBAND_COUNT_SHIFT) 
        /* As defined by the verilog implementation of the Control Register */
        #define heartBeat_DEADBAND_COUNT_SHIFT   0x00u 
    #endif /* (heartBeat_UsingFixedFunction) */
#endif /* (heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_256_CLOCKS) */



#if (heartBeat_UsingFixedFunction)
    #define heartBeat_STATUS                (*(reg8 *) heartBeat_PWMHW__SR0)
    #define heartBeat_STATUS_PTR            ((reg8 *) heartBeat_PWMHW__SR0)
    #define heartBeat_STATUS_MASK           (*(reg8 *) heartBeat_PWMHW__SR0)
    #define heartBeat_STATUS_MASK_PTR       ((reg8 *) heartBeat_PWMHW__SR0)
    #define heartBeat_CONTROL               (*(reg8 *) heartBeat_PWMHW__CFG0)
    #define heartBeat_CONTROL_PTR           ((reg8 *) heartBeat_PWMHW__CFG0)    
    #define heartBeat_CONTROL2              (*(reg8 *) heartBeat_PWMHW__CFG1)    
    #define heartBeat_CONTROL3              (*(reg8 *) heartBeat_PWMHW__CFG2)
    #define heartBeat_GLOBAL_ENABLE         (*(reg8 *) heartBeat_PWMHW__PM_ACT_CFG)
    #define heartBeat_GLOBAL_ENABLE_PTR       ( (reg8 *) heartBeat_PWMHW__PM_ACT_CFG)
    #define heartBeat_GLOBAL_STBY_ENABLE      (*(reg8 *) heartBeat_PWMHW__PM_STBY_CFG)
    #define heartBeat_GLOBAL_STBY_ENABLE_PTR  ( (reg8 *) heartBeat_PWMHW__PM_STBY_CFG)
  
  
    /***********************************
    *          Constants
    ***********************************/

    /* Fixed Function Block Chosen */
    #define heartBeat_BLOCK_EN_MASK          heartBeat_PWMHW__PM_ACT_MSK
    #define heartBeat_BLOCK_STBY_EN_MASK     heartBeat_PWMHW__PM_STBY_MSK 
    /* Control Register definitions */
    #define heartBeat_CTRL_ENABLE_SHIFT      0x00u
    
    #define heartBeat_CTRL_CMPMODE1_SHIFT    0x04u  /* As defined by Register map as MODE_CFG bits in CFG2*/
    
    #define heartBeat_CTRL_DEAD_TIME_SHIFT   0x06u   /* As defined by Register map */

    /* Fixed Function Block Only CFG register bit definitions */
    #define heartBeat_CFG0_MODE              0x02u   /*  Set to compare mode */
    
    /* #define heartBeat_CFG0_ENABLE            0x01u */  /* Enable the block to run */
    #define heartBeat_CFG0_DB                0x20u   /* As defined by Register map as DB bit in CFG0 */

    /* Control Register Bit Masks */
    #define heartBeat_CTRL_ENABLE            (uint8)((uint8)0x01u << heartBeat_CTRL_ENABLE_SHIFT)
    #define heartBeat_CTRL_RESET             (uint8)((uint8)0x01u << heartBeat_CTRL_RESET_SHIFT)
    #define heartBeat_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << heartBeat_CTRL_CMPMODE2_SHIFT)
    #define heartBeat_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << heartBeat_CTRL_CMPMODE1_SHIFT)
    
    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define heartBeat_CTRL2_IRQ_SEL_SHIFT    0x00u       
    #define heartBeat_CTRL2_IRQ_SEL          (uint8)((uint8)0x01u << heartBeat_CTRL2_IRQ_SEL_SHIFT)  
    
    /* Status Register Bit Locations */
    #define heartBeat_STATUS_TC_SHIFT            0x07u   /* As defined by Register map as TC in SR0 */
    #define heartBeat_STATUS_CMP1_SHIFT          0x06u   /* As defined by the Register map as CAP_CMP in SR0 */
    
    /* Status Register Interrupt Enable Bit Locations */
    #define heartBeat_STATUS_KILL_INT_EN_MASK_SHIFT          (0x00u)    
    #define heartBeat_STATUS_TC_INT_EN_MASK_SHIFT            (heartBeat_STATUS_TC_SHIFT - 4u)
    #define heartBeat_STATUS_CMP2_INT_EN_MASK_SHIFT          (0x00u)  
    #define heartBeat_STATUS_CMP1_INT_EN_MASK_SHIFT          (heartBeat_STATUS_CMP1_SHIFT - 4u)
    
    /* Status Register Bit Masks */
    #define heartBeat_STATUS_TC              (uint8)((uint8)0x01u << heartBeat_STATUS_TC_SHIFT)
    #define heartBeat_STATUS_CMP1            (uint8)((uint8)0x01u << heartBeat_STATUS_CMP1_SHIFT)
    
    /* Status Register Interrupt Bit Masks */
    #define heartBeat_STATUS_TC_INT_EN_MASK              (uint8)((uint8)heartBeat_STATUS_TC >> 4u)
    #define heartBeat_STATUS_CMP1_INT_EN_MASK            (uint8)((uint8)heartBeat_STATUS_CMP1 >> 4u)
    
    /*RT1 Synch Constants */
    #define heartBeat_RT1_SHIFT             0x04u
    #define heartBeat_RT1_MASK              (uint8)((uint8)0x03u << heartBeat_RT1_SHIFT)/* Sync TC and CMP bit masks */
    #define heartBeat_SYNC                  (uint8)((uint8)0x03u << heartBeat_RT1_SHIFT)
    #define heartBeat_SYNCDSI_SHIFT         0x00u
    #define heartBeat_SYNCDSI_MASK          (uint8)((uint8)0x0Fu << heartBeat_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    #define heartBeat_SYNCDSI_EN            (uint8)((uint8)0x0Fu << heartBeat_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    

#else
    #define heartBeat_STATUS                (*(reg8 *) heartBeat_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define heartBeat_STATUS_PTR            ((reg8 *) heartBeat_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define heartBeat_STATUS_MASK           (*(reg8 *) heartBeat_PWMUDB_genblk8_stsreg__MASK_REG)
    #define heartBeat_STATUS_MASK_PTR       ((reg8 *) heartBeat_PWMUDB_genblk8_stsreg__MASK_REG)
    #define heartBeat_STATUS_AUX_CTRL       (*(reg8 *) heartBeat_PWMUDB_genblk8_stsreg__STATUS_AUX_CTL_REG)
    #define heartBeat_CONTROL               (*(reg8 *) heartBeat_PWMUDB_genblk1_ctrlreg__CONTROL_REG)
    #define heartBeat_CONTROL_PTR           ((reg8 *) heartBeat_PWMUDB_genblk1_ctrlreg__CONTROL_REG)
    
    
    /***********************************
    *          Constants
    ***********************************/

    /* Control Register definitions */
    #define heartBeat_CTRL_ENABLE_SHIFT      0x07u
    #define heartBeat_CTRL_RESET_SHIFT       0x06u
    #define heartBeat_CTRL_CMPMODE2_SHIFT    0x03u
    #define heartBeat_CTRL_CMPMODE1_SHIFT    0x00u
    #define heartBeat_CTRL_DEAD_TIME_SHIFT   0x00u   /* No Shift Needed for UDB block */
    /* Control Register Bit Masks */
    #define heartBeat_CTRL_ENABLE            (uint8)((uint8)0x01u << heartBeat_CTRL_ENABLE_SHIFT)
    #define heartBeat_CTRL_RESET             (uint8)((uint8)0x01u << heartBeat_CTRL_RESET_SHIFT)
    #define heartBeat_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << heartBeat_CTRL_CMPMODE2_SHIFT)
    #define heartBeat_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << heartBeat_CTRL_CMPMODE1_SHIFT) 
    
    /* Status Register Bit Locations */
    #define heartBeat_STATUS_KILL_SHIFT          0x05u
    #define heartBeat_STATUS_FIFONEMPTY_SHIFT    0x04u
    #define heartBeat_STATUS_FIFOFULL_SHIFT      0x03u  
    #define heartBeat_STATUS_TC_SHIFT            0x02u
    #define heartBeat_STATUS_CMP2_SHIFT          0x01u
    #define heartBeat_STATUS_CMP1_SHIFT          0x00u
    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define heartBeat_STATUS_KILL_INT_EN_MASK_SHIFT          heartBeat_STATUS_KILL_SHIFT          
    #define heartBeat_STATUS_FIFONEMPTY_INT_EN_MASK_SHIFT    heartBeat_STATUS_FIFONEMPTY_SHIFT    
    #define heartBeat_STATUS_FIFOFULL_INT_EN_MASK_SHIFT      heartBeat_STATUS_FIFOFULL_SHIFT        
    #define heartBeat_STATUS_TC_INT_EN_MASK_SHIFT            heartBeat_STATUS_TC_SHIFT            
    #define heartBeat_STATUS_CMP2_INT_EN_MASK_SHIFT          heartBeat_STATUS_CMP2_SHIFT          
    #define heartBeat_STATUS_CMP1_INT_EN_MASK_SHIFT          heartBeat_STATUS_CMP1_SHIFT   
    /* Status Register Bit Masks */
    #define heartBeat_STATUS_KILL            (uint8)((uint8)0x00u << heartBeat_STATUS_KILL_SHIFT )
    #define heartBeat_STATUS_FIFOFULL        (uint8)((uint8)0x01u << heartBeat_STATUS_FIFOFULL_SHIFT)
    #define heartBeat_STATUS_FIFONEMPTY      (uint8)((uint8)0x01u << heartBeat_STATUS_FIFONEMPTY_SHIFT)
    #define heartBeat_STATUS_TC              (uint8)((uint8)0x01u << heartBeat_STATUS_TC_SHIFT)
    #define heartBeat_STATUS_CMP2            (uint8)((uint8)0x01u << heartBeat_STATUS_CMP2_SHIFT) 
    #define heartBeat_STATUS_CMP1            (uint8)((uint8)0x01u << heartBeat_STATUS_CMP1_SHIFT)
    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define heartBeat_STATUS_KILL_INT_EN_MASK            heartBeat_STATUS_KILL
    #define heartBeat_STATUS_FIFOFULL_INT_EN_MASK        heartBeat_STATUS_FIFOFULL
    #define heartBeat_STATUS_FIFONEMPTY_INT_EN_MASK      heartBeat_STATUS_FIFONEMPTY
    #define heartBeat_STATUS_TC_INT_EN_MASK              heartBeat_STATUS_TC
    #define heartBeat_STATUS_CMP2_INT_EN_MASK            heartBeat_STATUS_CMP2
    #define heartBeat_STATUS_CMP1_INT_EN_MASK            heartBeat_STATUS_CMP1
                                                          
    /* Datapath Auxillary Control Register definitions */
    #define heartBeat_AUX_CTRL_FIFO0_CLR     0x01u
    #define heartBeat_AUX_CTRL_FIFO1_CLR     0x02u
    #define heartBeat_AUX_CTRL_FIFO0_LVL     0x04u
    #define heartBeat_AUX_CTRL_FIFO1_LVL     0x08u
    #define heartBeat_STATUS_ACTL_INT_EN_MASK  0x10u /* As defined for the ACTL Register */
#endif /* heartBeat_UsingFixedFunction */

#endif  /* CY_PWM_heartBeat_H */


/* [] END OF FILE */
