/*******************************************************************************
* File Name: isr_triggerFiltering.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_ISR_isr_triggerFiltering_H)
#define CY_ISR_isr_triggerFiltering_H

#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void isr_triggerFiltering_Start(void) ;
void isr_triggerFiltering_StartEx(cyisraddress address) ;
void isr_triggerFiltering_Stop(void) ;

CY_ISR_PROTO(isr_triggerFiltering_Interrupt);

void isr_triggerFiltering_SetVector(cyisraddress address) ;
cyisraddress isr_triggerFiltering_GetVector(void) ;

void isr_triggerFiltering_SetPriority(uint8 priority) ;
uint8 isr_triggerFiltering_GetPriority(void) ;

void isr_triggerFiltering_Enable(void) ;
uint8 isr_triggerFiltering_GetState(void) ;
void isr_triggerFiltering_Disable(void) ;

void isr_triggerFiltering_SetPending(void) ;
void isr_triggerFiltering_ClearPending(void) ;


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the isr_triggerFiltering ISR. */
#define isr_triggerFiltering_INTC_VECTOR            ((reg16 *) isr_triggerFiltering__INTC_VECT)

/* Address of the isr_triggerFiltering ISR priority. */
#define isr_triggerFiltering_INTC_PRIOR             ((reg8 *) isr_triggerFiltering__INTC_PRIOR_REG)

/* Priority of the isr_triggerFiltering interrupt. */
#define isr_triggerFiltering_INTC_PRIOR_NUMBER      isr_triggerFiltering__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable isr_triggerFiltering interrupt. */
#define isr_triggerFiltering_INTC_SET_EN            ((reg8 *) isr_triggerFiltering__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the isr_triggerFiltering interrupt. */
#define isr_triggerFiltering_INTC_CLR_EN            ((reg8 *) isr_triggerFiltering__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the isr_triggerFiltering interrupt state to pending. */
#define isr_triggerFiltering_INTC_SET_PD            ((reg8 *) isr_triggerFiltering__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the isr_triggerFiltering interrupt. */
#define isr_triggerFiltering_INTC_CLR_PD            ((reg8 *) isr_triggerFiltering__INTC_CLR_PD_REG)



#endif /* CY_ISR_isr_triggerFiltering_H */


/* [] END OF FILE */
