/*******************************************************************************
* File Name: PWM_heartBeat_PM.c
* Version 3.0
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/
#include "cytypes.h"
#include "PWM_heartBeat.h"

static PWM_heartBeat_backupStruct PWM_heartBeat_backup;


/*******************************************************************************
* Function Name: PWM_heartBeat_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_heartBeat_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void PWM_heartBeat_SaveConfig(void) 
{
    
    #if(!PWM_heartBeat_UsingFixedFunction)
        #if(!PWM_heartBeat_PWMModeIsCenterAligned)
            PWM_heartBeat_backup.PWMPeriod = PWM_heartBeat_ReadPeriod();
        #endif /* (!PWM_heartBeat_PWMModeIsCenterAligned) */
        PWM_heartBeat_backup.PWMUdb = PWM_heartBeat_ReadCounter();
        #if (PWM_heartBeat_UseStatus)
            PWM_heartBeat_backup.InterruptMaskValue = PWM_heartBeat_STATUS_MASK;
        #endif /* (PWM_heartBeat_UseStatus) */
        
        #if(PWM_heartBeat_DeadBandMode == PWM_heartBeat__B_PWM__DBM_256_CLOCKS || \
            PWM_heartBeat_DeadBandMode == PWM_heartBeat__B_PWM__DBM_2_4_CLOCKS)
            PWM_heartBeat_backup.PWMdeadBandValue = PWM_heartBeat_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */
        
        #if(PWM_heartBeat_KillModeMinTime)
             PWM_heartBeat_backup.PWMKillCounterPeriod = PWM_heartBeat_ReadKillTime();
        #endif /* (PWM_heartBeat_KillModeMinTime) */
        
        #if(PWM_heartBeat_UseControl)
            PWM_heartBeat_backup.PWMControlRegister = PWM_heartBeat_ReadControlRegister();
        #endif /* (PWM_heartBeat_UseControl) */
    #endif  /* (!PWM_heartBeat_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_heartBeat_RestoreConfig
********************************************************************************
* 
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_heartBeat_backup:  Variables of this global structure are used to  
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_heartBeat_RestoreConfig(void) 
{
        #if(!PWM_heartBeat_UsingFixedFunction)
            #if(!PWM_heartBeat_PWMModeIsCenterAligned)
                PWM_heartBeat_WritePeriod(PWM_heartBeat_backup.PWMPeriod);
            #endif /* (!PWM_heartBeat_PWMModeIsCenterAligned) */
            PWM_heartBeat_WriteCounter(PWM_heartBeat_backup.PWMUdb);
            #if (PWM_heartBeat_UseStatus)
                PWM_heartBeat_STATUS_MASK = PWM_heartBeat_backup.InterruptMaskValue;
            #endif /* (PWM_heartBeat_UseStatus) */
            
            #if(PWM_heartBeat_DeadBandMode == PWM_heartBeat__B_PWM__DBM_256_CLOCKS || \
                PWM_heartBeat_DeadBandMode == PWM_heartBeat__B_PWM__DBM_2_4_CLOCKS)
                PWM_heartBeat_WriteDeadTime(PWM_heartBeat_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */
            
            #if(PWM_heartBeat_KillModeMinTime)
                PWM_heartBeat_WriteKillTime(PWM_heartBeat_backup.PWMKillCounterPeriod);
            #endif /* (PWM_heartBeat_KillModeMinTime) */
            
            #if(PWM_heartBeat_UseControl)
                PWM_heartBeat_WriteControlRegister(PWM_heartBeat_backup.PWMControlRegister); 
            #endif /* (PWM_heartBeat_UseControl) */
        #endif  /* (!PWM_heartBeat_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_heartBeat_Sleep
********************************************************************************
* 
* Summary:
*  Disables block's operation and saves the user configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_heartBeat_backup.PWMEnableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_heartBeat_Sleep(void) 
{
    #if(PWM_heartBeat_UseControl)
        if(PWM_heartBeat_CTRL_ENABLE == (PWM_heartBeat_CONTROL & PWM_heartBeat_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_heartBeat_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_heartBeat_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_heartBeat_UseControl) */

    /* Stop component */
    PWM_heartBeat_Stop();
    
    /* Save registers configuration */
    PWM_heartBeat_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_heartBeat_Wakeup
********************************************************************************
* 
* Summary:
*  Restores and enables the user configuration. Should be called just after 
*  awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  PWM_heartBeat_backup.pwmEnable:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_heartBeat_Wakeup(void) 
{
     /* Restore registers values */
    PWM_heartBeat_RestoreConfig();
    
    if(PWM_heartBeat_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_heartBeat_Enable();
    } /* Do nothing if component's block was disabled before */
    
}


/* [] END OF FILE */
