/*******************************************************************************
* File Name: SPIM_opticalDriver_2_INT.c
* Version 2.40
*
* Description:
*  This file provides all Interrupt Service Routine (ISR) for the SPI Master
*  component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "SPIM_opticalDriver_2_PVT.h"

/* User code required at start of ISR */
/* `#START SPIM_opticalDriver_2_ISR_START_DEF` */

/* `#END` */


/*******************************************************************************
* Function Name: SPIM_opticalDriver_2_TX_ISR
********************************************************************************
*
* Summary:
*  Interrupt Service Routine for TX portion of the SPI Master.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  SPIM_opticalDriver_2_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer.
*  SPIM_opticalDriver_2_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer, modified when exist data to
*  sending and FIFO Not Full.
*  SPIM_opticalDriver_2_txBuffer[SPIM_opticalDriver_2_TX_BUFFER_SIZE] - used to store
*  data to sending.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(SPIM_opticalDriver_2_TX_ISR)
{
    #if(SPIM_opticalDriver_2_TX_SOFTWARE_BUF_ENABLED)
        uint8 tmpStatus;
    #endif /* (SPIM_opticalDriver_2_TX_SOFTWARE_BUF_ENABLED) */

    /* User code required at start of ISR */
    /* `#START SPIM_opticalDriver_2_TX_ISR_START` */

    /* `#END` */

    #if(SPIM_opticalDriver_2_TX_SOFTWARE_BUF_ENABLED)
        /* Check if TX data buffer is not empty and there is space in TX FIFO */
        while(SPIM_opticalDriver_2_txBufferRead != SPIM_opticalDriver_2_txBufferWrite)
        {
            tmpStatus = SPIM_opticalDriver_2_GET_STATUS_TX(SPIM_opticalDriver_2_swStatusTx);
            SPIM_opticalDriver_2_swStatusTx = tmpStatus;

            if(0u != (SPIM_opticalDriver_2_swStatusTx & SPIM_opticalDriver_2_STS_TX_FIFO_NOT_FULL))
            {
                if(0u == SPIM_opticalDriver_2_txBufferFull)
                {
                   SPIM_opticalDriver_2_txBufferRead++;

                    if(SPIM_opticalDriver_2_txBufferRead >= SPIM_opticalDriver_2_TX_BUFFER_SIZE)
                    {
                        SPIM_opticalDriver_2_txBufferRead = 0u;
                    }
                }
                else
                {
                    SPIM_opticalDriver_2_txBufferFull = 0u;
                }

                /* Move data from the Buffer to the FIFO */
                CY_SET_REG8(SPIM_opticalDriver_2_TXDATA_PTR,
                    SPIM_opticalDriver_2_txBuffer[SPIM_opticalDriver_2_txBufferRead]);
            }
            else
            {
                break;
            }
        }

        if(SPIM_opticalDriver_2_txBufferRead == SPIM_opticalDriver_2_txBufferWrite)
        {
            /* TX Buffer is EMPTY: disable interrupt on TX NOT FULL */
            SPIM_opticalDriver_2_TX_STATUS_MASK_REG &= ((uint8) ~SPIM_opticalDriver_2_STS_TX_FIFO_NOT_FULL);
        }

    #endif /* (SPIM_opticalDriver_2_TX_SOFTWARE_BUF_ENABLED) */

    /* User code required at end of ISR (Optional) */
    /* `#START SPIM_opticalDriver_2_TX_ISR_END` */

    /* `#END` */
}


/*******************************************************************************
* Function Name: SPIM_opticalDriver_2_RX_ISR
********************************************************************************
*
* Summary:
*  Interrupt Service Routine for RX portion of the SPI Master.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  SPIM_opticalDriver_2_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer modified when FIFO contains
*  new data.
*  SPIM_opticalDriver_2_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified when overflow occurred.
*  SPIM_opticalDriver_2_rxBuffer[SPIM_opticalDriver_2_RX_BUFFER_SIZE] - used to store
*  received data, modified when FIFO contains new data.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(SPIM_opticalDriver_2_RX_ISR)
{
    #if(SPIM_opticalDriver_2_RX_SOFTWARE_BUF_ENABLED)
        uint8 tmpStatus;
        uint8 rxData;
    #endif /* (SPIM_opticalDriver_2_RX_SOFTWARE_BUF_ENABLED) */

    /* User code required at start of ISR */
    /* `#START SPIM_opticalDriver_2_RX_ISR_START` */

    /* `#END` */

    #if(SPIM_opticalDriver_2_RX_SOFTWARE_BUF_ENABLED)

        tmpStatus = SPIM_opticalDriver_2_GET_STATUS_RX(SPIM_opticalDriver_2_swStatusRx);
        SPIM_opticalDriver_2_swStatusRx = tmpStatus;

        /* Check if RX data FIFO has some data to be moved into the RX Buffer */
        while(0u != (SPIM_opticalDriver_2_swStatusRx & SPIM_opticalDriver_2_STS_RX_FIFO_NOT_EMPTY))
        {
            rxData = CY_GET_REG8(SPIM_opticalDriver_2_RXDATA_PTR);

            /* Set next pointer. */
            SPIM_opticalDriver_2_rxBufferWrite++;
            if(SPIM_opticalDriver_2_rxBufferWrite >= SPIM_opticalDriver_2_RX_BUFFER_SIZE)
            {
                SPIM_opticalDriver_2_rxBufferWrite = 0u;
            }

            if(SPIM_opticalDriver_2_rxBufferWrite == SPIM_opticalDriver_2_rxBufferRead)
            {
                SPIM_opticalDriver_2_rxBufferRead++;
                if(SPIM_opticalDriver_2_rxBufferRead >= SPIM_opticalDriver_2_RX_BUFFER_SIZE)
                {
                    SPIM_opticalDriver_2_rxBufferRead = 0u;
                }

                SPIM_opticalDriver_2_rxBufferFull = 1u;
            }

            /* Move data from the FIFO to the Buffer */
            SPIM_opticalDriver_2_rxBuffer[SPIM_opticalDriver_2_rxBufferWrite] = rxData;

            tmpStatus = SPIM_opticalDriver_2_GET_STATUS_RX(SPIM_opticalDriver_2_swStatusRx);
            SPIM_opticalDriver_2_swStatusRx = tmpStatus;
        }

    #endif /* (SPIM_opticalDriver_2_RX_SOFTWARE_BUF_ENABLED) */

    /* User code required at end of ISR (Optional) */
    /* `#START SPIM_opticalDriver_2_RX_ISR_END` */

    /* `#END` */
}

/* [] END OF FILE */
