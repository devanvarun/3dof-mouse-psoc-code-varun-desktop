/*******************************************************************************
* File Name: opticalDriver_NCS.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "opticalDriver_NCS.h"


/*******************************************************************************
* Function Name: opticalDriver_NCS_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void opticalDriver_NCS_Write(uint8 value) 
{
    uint8 staticBits = (opticalDriver_NCS_DR & (uint8)(~opticalDriver_NCS_MASK));
    opticalDriver_NCS_DR = staticBits | ((uint8)(value << opticalDriver_NCS_SHIFT) & opticalDriver_NCS_MASK);
}


/*******************************************************************************
* Function Name: opticalDriver_NCS_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void opticalDriver_NCS_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(opticalDriver_NCS_0, mode);
}


/*******************************************************************************
* Function Name: opticalDriver_NCS_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro opticalDriver_NCS_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 opticalDriver_NCS_Read(void) 
{
    return (opticalDriver_NCS_PS & opticalDriver_NCS_MASK) >> opticalDriver_NCS_SHIFT;
}


/*******************************************************************************
* Function Name: opticalDriver_NCS_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 opticalDriver_NCS_ReadDataReg(void) 
{
    return (opticalDriver_NCS_DR & opticalDriver_NCS_MASK) >> opticalDriver_NCS_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(opticalDriver_NCS_INTSTAT) 

    /*******************************************************************************
    * Function Name: opticalDriver_NCS_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 opticalDriver_NCS_ClearInterrupt(void) 
    {
        return (opticalDriver_NCS_INTSTAT & opticalDriver_NCS_MASK) >> opticalDriver_NCS_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
