/*******************************************************************************
* File Name: opticalDriver_MOSI_2.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_opticalDriver_MOSI_2_H) /* Pins opticalDriver_MOSI_2_H */
#define CY_PINS_opticalDriver_MOSI_2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "opticalDriver_MOSI_2_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    opticalDriver_MOSI_2_Write(uint8 value) ;
void    opticalDriver_MOSI_2_SetDriveMode(uint8 mode) ;
uint8   opticalDriver_MOSI_2_ReadDataReg(void) ;
uint8   opticalDriver_MOSI_2_Read(void) ;
uint8   opticalDriver_MOSI_2_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define opticalDriver_MOSI_2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define opticalDriver_MOSI_2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define opticalDriver_MOSI_2_DM_RES_UP          PIN_DM_RES_UP
#define opticalDriver_MOSI_2_DM_RES_DWN         PIN_DM_RES_DWN
#define opticalDriver_MOSI_2_DM_OD_LO           PIN_DM_OD_LO
#define opticalDriver_MOSI_2_DM_OD_HI           PIN_DM_OD_HI
#define opticalDriver_MOSI_2_DM_STRONG          PIN_DM_STRONG
#define opticalDriver_MOSI_2_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define opticalDriver_MOSI_2_MASK               opticalDriver_MOSI_2__MASK
#define opticalDriver_MOSI_2_SHIFT              opticalDriver_MOSI_2__SHIFT
#define opticalDriver_MOSI_2_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define opticalDriver_MOSI_2_PS                     (* (reg8 *) opticalDriver_MOSI_2__PS)
/* Data Register */
#define opticalDriver_MOSI_2_DR                     (* (reg8 *) opticalDriver_MOSI_2__DR)
/* Port Number */
#define opticalDriver_MOSI_2_PRT_NUM                (* (reg8 *) opticalDriver_MOSI_2__PRT) 
/* Connect to Analog Globals */                                                  
#define opticalDriver_MOSI_2_AG                     (* (reg8 *) opticalDriver_MOSI_2__AG)                       
/* Analog MUX bux enable */
#define opticalDriver_MOSI_2_AMUX                   (* (reg8 *) opticalDriver_MOSI_2__AMUX) 
/* Bidirectional Enable */                                                        
#define opticalDriver_MOSI_2_BIE                    (* (reg8 *) opticalDriver_MOSI_2__BIE)
/* Bit-mask for Aliased Register Access */
#define opticalDriver_MOSI_2_BIT_MASK               (* (reg8 *) opticalDriver_MOSI_2__BIT_MASK)
/* Bypass Enable */
#define opticalDriver_MOSI_2_BYP                    (* (reg8 *) opticalDriver_MOSI_2__BYP)
/* Port wide control signals */                                                   
#define opticalDriver_MOSI_2_CTL                    (* (reg8 *) opticalDriver_MOSI_2__CTL)
/* Drive Modes */
#define opticalDriver_MOSI_2_DM0                    (* (reg8 *) opticalDriver_MOSI_2__DM0) 
#define opticalDriver_MOSI_2_DM1                    (* (reg8 *) opticalDriver_MOSI_2__DM1)
#define opticalDriver_MOSI_2_DM2                    (* (reg8 *) opticalDriver_MOSI_2__DM2) 
/* Input Buffer Disable Override */
#define opticalDriver_MOSI_2_INP_DIS                (* (reg8 *) opticalDriver_MOSI_2__INP_DIS)
/* LCD Common or Segment Drive */
#define opticalDriver_MOSI_2_LCD_COM_SEG            (* (reg8 *) opticalDriver_MOSI_2__LCD_COM_SEG)
/* Enable Segment LCD */
#define opticalDriver_MOSI_2_LCD_EN                 (* (reg8 *) opticalDriver_MOSI_2__LCD_EN)
/* Slew Rate Control */
#define opticalDriver_MOSI_2_SLW                    (* (reg8 *) opticalDriver_MOSI_2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define opticalDriver_MOSI_2_PRTDSI__CAPS_SEL       (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define opticalDriver_MOSI_2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define opticalDriver_MOSI_2_PRTDSI__OE_SEL0        (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__OE_SEL0) 
#define opticalDriver_MOSI_2_PRTDSI__OE_SEL1        (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define opticalDriver_MOSI_2_PRTDSI__OUT_SEL0       (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__OUT_SEL0) 
#define opticalDriver_MOSI_2_PRTDSI__OUT_SEL1       (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define opticalDriver_MOSI_2_PRTDSI__SYNC_OUT       (* (reg8 *) opticalDriver_MOSI_2__PRTDSI__SYNC_OUT) 


#if defined(opticalDriver_MOSI_2__INTSTAT)  /* Interrupt Registers */

    #define opticalDriver_MOSI_2_INTSTAT                (* (reg8 *) opticalDriver_MOSI_2__INTSTAT)
    #define opticalDriver_MOSI_2_SNAP                   (* (reg8 *) opticalDriver_MOSI_2__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins opticalDriver_MOSI_2_H */


/* [] END OF FILE */
