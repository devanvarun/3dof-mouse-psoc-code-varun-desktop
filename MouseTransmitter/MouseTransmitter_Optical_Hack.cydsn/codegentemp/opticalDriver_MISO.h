/*******************************************************************************
* File Name: opticalDriver_MISO.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_opticalDriver_MISO_H) /* Pins opticalDriver_MISO_H */
#define CY_PINS_opticalDriver_MISO_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "opticalDriver_MISO_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    opticalDriver_MISO_Write(uint8 value) ;
void    opticalDriver_MISO_SetDriveMode(uint8 mode) ;
uint8   opticalDriver_MISO_ReadDataReg(void) ;
uint8   opticalDriver_MISO_Read(void) ;
uint8   opticalDriver_MISO_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define opticalDriver_MISO_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define opticalDriver_MISO_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define opticalDriver_MISO_DM_RES_UP          PIN_DM_RES_UP
#define opticalDriver_MISO_DM_RES_DWN         PIN_DM_RES_DWN
#define opticalDriver_MISO_DM_OD_LO           PIN_DM_OD_LO
#define opticalDriver_MISO_DM_OD_HI           PIN_DM_OD_HI
#define opticalDriver_MISO_DM_STRONG          PIN_DM_STRONG
#define opticalDriver_MISO_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define opticalDriver_MISO_MASK               opticalDriver_MISO__MASK
#define opticalDriver_MISO_SHIFT              opticalDriver_MISO__SHIFT
#define opticalDriver_MISO_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define opticalDriver_MISO_PS                     (* (reg8 *) opticalDriver_MISO__PS)
/* Data Register */
#define opticalDriver_MISO_DR                     (* (reg8 *) opticalDriver_MISO__DR)
/* Port Number */
#define opticalDriver_MISO_PRT_NUM                (* (reg8 *) opticalDriver_MISO__PRT) 
/* Connect to Analog Globals */                                                  
#define opticalDriver_MISO_AG                     (* (reg8 *) opticalDriver_MISO__AG)                       
/* Analog MUX bux enable */
#define opticalDriver_MISO_AMUX                   (* (reg8 *) opticalDriver_MISO__AMUX) 
/* Bidirectional Enable */                                                        
#define opticalDriver_MISO_BIE                    (* (reg8 *) opticalDriver_MISO__BIE)
/* Bit-mask for Aliased Register Access */
#define opticalDriver_MISO_BIT_MASK               (* (reg8 *) opticalDriver_MISO__BIT_MASK)
/* Bypass Enable */
#define opticalDriver_MISO_BYP                    (* (reg8 *) opticalDriver_MISO__BYP)
/* Port wide control signals */                                                   
#define opticalDriver_MISO_CTL                    (* (reg8 *) opticalDriver_MISO__CTL)
/* Drive Modes */
#define opticalDriver_MISO_DM0                    (* (reg8 *) opticalDriver_MISO__DM0) 
#define opticalDriver_MISO_DM1                    (* (reg8 *) opticalDriver_MISO__DM1)
#define opticalDriver_MISO_DM2                    (* (reg8 *) opticalDriver_MISO__DM2) 
/* Input Buffer Disable Override */
#define opticalDriver_MISO_INP_DIS                (* (reg8 *) opticalDriver_MISO__INP_DIS)
/* LCD Common or Segment Drive */
#define opticalDriver_MISO_LCD_COM_SEG            (* (reg8 *) opticalDriver_MISO__LCD_COM_SEG)
/* Enable Segment LCD */
#define opticalDriver_MISO_LCD_EN                 (* (reg8 *) opticalDriver_MISO__LCD_EN)
/* Slew Rate Control */
#define opticalDriver_MISO_SLW                    (* (reg8 *) opticalDriver_MISO__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define opticalDriver_MISO_PRTDSI__CAPS_SEL       (* (reg8 *) opticalDriver_MISO__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define opticalDriver_MISO_PRTDSI__DBL_SYNC_IN    (* (reg8 *) opticalDriver_MISO__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define opticalDriver_MISO_PRTDSI__OE_SEL0        (* (reg8 *) opticalDriver_MISO__PRTDSI__OE_SEL0) 
#define opticalDriver_MISO_PRTDSI__OE_SEL1        (* (reg8 *) opticalDriver_MISO__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define opticalDriver_MISO_PRTDSI__OUT_SEL0       (* (reg8 *) opticalDriver_MISO__PRTDSI__OUT_SEL0) 
#define opticalDriver_MISO_PRTDSI__OUT_SEL1       (* (reg8 *) opticalDriver_MISO__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define opticalDriver_MISO_PRTDSI__SYNC_OUT       (* (reg8 *) opticalDriver_MISO__PRTDSI__SYNC_OUT) 


#if defined(opticalDriver_MISO__INTSTAT)  /* Interrupt Registers */

    #define opticalDriver_MISO_INTSTAT                (* (reg8 *) opticalDriver_MISO__INTSTAT)
    #define opticalDriver_MISO_SNAP                   (* (reg8 *) opticalDriver_MISO__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins opticalDriver_MISO_H */


/* [] END OF FILE */
