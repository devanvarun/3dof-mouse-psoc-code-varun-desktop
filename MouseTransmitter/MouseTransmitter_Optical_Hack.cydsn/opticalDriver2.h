/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "cytypes.h"

#define REG_PRODUCT_ID 0x00
#define REG_REVISION_ID 0x01
#define REG_MOTION 0x02
#define REG_DELTA_X 0x03
#define REG_DELTA_Y 0x04
#define REG_SQUAL 0x05
#define REG_SHUTTER_UPPER 0x06
#define REG_SHUTTER_LOWER 0x07
#define REG_MAXIMUM_PIXEL 0x08
#define REG_PIXEL_SUM 0x09
#define REG_MINIMUM_PIXEL 0x0A
#define REG_PIXEL_GRAB 0x0B
#define REG_MOUSE_CONTROL 0x0D
#define REG_CHIP_RESET 0x3A
#define REG_INV_REV_ID 0x3f
#define REG_SENSOR_CURRENT_SETTING 0x40
#define REG_REST_MODE_CONFIGURATION 0x45
#define REG_MOTION_BURST 0x63

#define MOUSE_MOVING 0x01
#define MOUSE_NOT_MOVING 0x00

void NCS_HIGH_2   (void);
// Chip Select -> low
void  NCS_LOW_2  (void);
uint8 SPI_opticalDriver_2_SendByte(uint8 spiData);
void setupOpticalSensor_2(void);
uint8 opticalDriverGetDeltaX_2(void);
uint8 opticalDriverGetDeltaY_2(void);
uint8 opticalDriverGetProductId_2(void);
uint8 opticalDriverIsMouseMoving_2(void);

/* [] END OF FILE */
