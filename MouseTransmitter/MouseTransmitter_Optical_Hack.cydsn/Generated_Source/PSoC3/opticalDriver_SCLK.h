/*******************************************************************************
* File Name: opticalDriver_SCLK.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_opticalDriver_SCLK_H) /* Pins opticalDriver_SCLK_H */
#define CY_PINS_opticalDriver_SCLK_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "opticalDriver_SCLK_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    opticalDriver_SCLK_Write(uint8 value) ;
void    opticalDriver_SCLK_SetDriveMode(uint8 mode) ;
uint8   opticalDriver_SCLK_ReadDataReg(void) ;
uint8   opticalDriver_SCLK_Read(void) ;
uint8   opticalDriver_SCLK_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define opticalDriver_SCLK_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define opticalDriver_SCLK_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define opticalDriver_SCLK_DM_RES_UP          PIN_DM_RES_UP
#define opticalDriver_SCLK_DM_RES_DWN         PIN_DM_RES_DWN
#define opticalDriver_SCLK_DM_OD_LO           PIN_DM_OD_LO
#define opticalDriver_SCLK_DM_OD_HI           PIN_DM_OD_HI
#define opticalDriver_SCLK_DM_STRONG          PIN_DM_STRONG
#define opticalDriver_SCLK_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define opticalDriver_SCLK_MASK               opticalDriver_SCLK__MASK
#define opticalDriver_SCLK_SHIFT              opticalDriver_SCLK__SHIFT
#define opticalDriver_SCLK_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define opticalDriver_SCLK_PS                     (* (reg8 *) opticalDriver_SCLK__PS)
/* Data Register */
#define opticalDriver_SCLK_DR                     (* (reg8 *) opticalDriver_SCLK__DR)
/* Port Number */
#define opticalDriver_SCLK_PRT_NUM                (* (reg8 *) opticalDriver_SCLK__PRT) 
/* Connect to Analog Globals */                                                  
#define opticalDriver_SCLK_AG                     (* (reg8 *) opticalDriver_SCLK__AG)                       
/* Analog MUX bux enable */
#define opticalDriver_SCLK_AMUX                   (* (reg8 *) opticalDriver_SCLK__AMUX) 
/* Bidirectional Enable */                                                        
#define opticalDriver_SCLK_BIE                    (* (reg8 *) opticalDriver_SCLK__BIE)
/* Bit-mask for Aliased Register Access */
#define opticalDriver_SCLK_BIT_MASK               (* (reg8 *) opticalDriver_SCLK__BIT_MASK)
/* Bypass Enable */
#define opticalDriver_SCLK_BYP                    (* (reg8 *) opticalDriver_SCLK__BYP)
/* Port wide control signals */                                                   
#define opticalDriver_SCLK_CTL                    (* (reg8 *) opticalDriver_SCLK__CTL)
/* Drive Modes */
#define opticalDriver_SCLK_DM0                    (* (reg8 *) opticalDriver_SCLK__DM0) 
#define opticalDriver_SCLK_DM1                    (* (reg8 *) opticalDriver_SCLK__DM1)
#define opticalDriver_SCLK_DM2                    (* (reg8 *) opticalDriver_SCLK__DM2) 
/* Input Buffer Disable Override */
#define opticalDriver_SCLK_INP_DIS                (* (reg8 *) opticalDriver_SCLK__INP_DIS)
/* LCD Common or Segment Drive */
#define opticalDriver_SCLK_LCD_COM_SEG            (* (reg8 *) opticalDriver_SCLK__LCD_COM_SEG)
/* Enable Segment LCD */
#define opticalDriver_SCLK_LCD_EN                 (* (reg8 *) opticalDriver_SCLK__LCD_EN)
/* Slew Rate Control */
#define opticalDriver_SCLK_SLW                    (* (reg8 *) opticalDriver_SCLK__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define opticalDriver_SCLK_PRTDSI__CAPS_SEL       (* (reg8 *) opticalDriver_SCLK__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define opticalDriver_SCLK_PRTDSI__DBL_SYNC_IN    (* (reg8 *) opticalDriver_SCLK__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define opticalDriver_SCLK_PRTDSI__OE_SEL0        (* (reg8 *) opticalDriver_SCLK__PRTDSI__OE_SEL0) 
#define opticalDriver_SCLK_PRTDSI__OE_SEL1        (* (reg8 *) opticalDriver_SCLK__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define opticalDriver_SCLK_PRTDSI__OUT_SEL0       (* (reg8 *) opticalDriver_SCLK__PRTDSI__OUT_SEL0) 
#define opticalDriver_SCLK_PRTDSI__OUT_SEL1       (* (reg8 *) opticalDriver_SCLK__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define opticalDriver_SCLK_PRTDSI__SYNC_OUT       (* (reg8 *) opticalDriver_SCLK__PRTDSI__SYNC_OUT) 


#if defined(opticalDriver_SCLK__INTSTAT)  /* Interrupt Registers */

    #define opticalDriver_SCLK_INTSTAT                (* (reg8 *) opticalDriver_SCLK__INTSTAT)
    #define opticalDriver_SCLK_SNAP                   (* (reg8 *) opticalDriver_SCLK__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins opticalDriver_SCLK_H */


/* [] END OF FILE */
