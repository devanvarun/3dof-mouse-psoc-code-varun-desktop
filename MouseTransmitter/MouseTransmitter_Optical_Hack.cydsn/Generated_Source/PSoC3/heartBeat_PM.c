/*******************************************************************************
* File Name: heartBeat_PM.c
* Version 3.0
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/
#include "cytypes.h"
#include "heartBeat.h"

static heartBeat_backupStruct heartBeat_backup;


/*******************************************************************************
* Function Name: heartBeat_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  heartBeat_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void heartBeat_SaveConfig(void) 
{
    
    #if(!heartBeat_UsingFixedFunction)
        #if(!heartBeat_PWMModeIsCenterAligned)
            heartBeat_backup.PWMPeriod = heartBeat_ReadPeriod();
        #endif /* (!heartBeat_PWMModeIsCenterAligned) */
        heartBeat_backup.PWMUdb = heartBeat_ReadCounter();
        #if (heartBeat_UseStatus)
            heartBeat_backup.InterruptMaskValue = heartBeat_STATUS_MASK;
        #endif /* (heartBeat_UseStatus) */
        
        #if(heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_256_CLOCKS || \
            heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_2_4_CLOCKS)
            heartBeat_backup.PWMdeadBandValue = heartBeat_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */
        
        #if(heartBeat_KillModeMinTime)
             heartBeat_backup.PWMKillCounterPeriod = heartBeat_ReadKillTime();
        #endif /* (heartBeat_KillModeMinTime) */
        
        #if(heartBeat_UseControl)
            heartBeat_backup.PWMControlRegister = heartBeat_ReadControlRegister();
        #endif /* (heartBeat_UseControl) */
    #endif  /* (!heartBeat_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: heartBeat_RestoreConfig
********************************************************************************
* 
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  heartBeat_backup:  Variables of this global structure are used to  
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void heartBeat_RestoreConfig(void) 
{
        #if(!heartBeat_UsingFixedFunction)
            #if(!heartBeat_PWMModeIsCenterAligned)
                heartBeat_WritePeriod(heartBeat_backup.PWMPeriod);
            #endif /* (!heartBeat_PWMModeIsCenterAligned) */
            heartBeat_WriteCounter(heartBeat_backup.PWMUdb);
            #if (heartBeat_UseStatus)
                heartBeat_STATUS_MASK = heartBeat_backup.InterruptMaskValue;
            #endif /* (heartBeat_UseStatus) */
            
            #if(heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_256_CLOCKS || \
                heartBeat_DeadBandMode == heartBeat__B_PWM__DBM_2_4_CLOCKS)
                heartBeat_WriteDeadTime(heartBeat_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */
            
            #if(heartBeat_KillModeMinTime)
                heartBeat_WriteKillTime(heartBeat_backup.PWMKillCounterPeriod);
            #endif /* (heartBeat_KillModeMinTime) */
            
            #if(heartBeat_UseControl)
                heartBeat_WriteControlRegister(heartBeat_backup.PWMControlRegister); 
            #endif /* (heartBeat_UseControl) */
        #endif  /* (!heartBeat_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: heartBeat_Sleep
********************************************************************************
* 
* Summary:
*  Disables block's operation and saves the user configuration. Should be called 
*  just prior to entering sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  heartBeat_backup.PWMEnableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void heartBeat_Sleep(void) 
{
    #if(heartBeat_UseControl)
        if(heartBeat_CTRL_ENABLE == (heartBeat_CONTROL & heartBeat_CTRL_ENABLE))
        {
            /*Component is enabled */
            heartBeat_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            heartBeat_backup.PWMEnableState = 0u;
        }
    #endif /* (heartBeat_UseControl) */

    /* Stop component */
    heartBeat_Stop();
    
    /* Save registers configuration */
    heartBeat_SaveConfig();
}


/*******************************************************************************
* Function Name: heartBeat_Wakeup
********************************************************************************
* 
* Summary:
*  Restores and enables the user configuration. Should be called just after 
*  awaking from sleep.
*  
* Parameters:  
*  None
*
* Return: 
*  None
*
* Global variables:
*  heartBeat_backup.pwmEnable:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void heartBeat_Wakeup(void) 
{
     /* Restore registers values */
    heartBeat_RestoreConfig();
    
    if(heartBeat_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        heartBeat_Enable();
    } /* Do nothing if component's block was disabled before */
    
}


/* [] END OF FILE */
