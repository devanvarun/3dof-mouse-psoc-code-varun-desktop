/*******************************************************************************
* File Name: opticalDriver_Reset.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_opticalDriver_Reset_ALIASES_H) /* Pins opticalDriver_Reset_ALIASES_H */
#define CY_PINS_opticalDriver_Reset_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define opticalDriver_Reset_0		opticalDriver_Reset__0__PC

#endif /* End Pins opticalDriver_Reset_ALIASES_H */


/* [] END OF FILE */
