/*******************************************************************************
* File Name: MISO_opticalDriver.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MISO_opticalDriver_H) /* Pins MISO_opticalDriver_H */
#define CY_PINS_MISO_opticalDriver_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MISO_opticalDriver_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    MISO_opticalDriver_Write(uint8 value) ;
void    MISO_opticalDriver_SetDriveMode(uint8 mode) ;
uint8   MISO_opticalDriver_ReadDataReg(void) ;
uint8   MISO_opticalDriver_Read(void) ;
uint8   MISO_opticalDriver_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MISO_opticalDriver_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MISO_opticalDriver_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MISO_opticalDriver_DM_RES_UP          PIN_DM_RES_UP
#define MISO_opticalDriver_DM_RES_DWN         PIN_DM_RES_DWN
#define MISO_opticalDriver_DM_OD_LO           PIN_DM_OD_LO
#define MISO_opticalDriver_DM_OD_HI           PIN_DM_OD_HI
#define MISO_opticalDriver_DM_STRONG          PIN_DM_STRONG
#define MISO_opticalDriver_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MISO_opticalDriver_MASK               MISO_opticalDriver__MASK
#define MISO_opticalDriver_SHIFT              MISO_opticalDriver__SHIFT
#define MISO_opticalDriver_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MISO_opticalDriver_PS                     (* (reg8 *) MISO_opticalDriver__PS)
/* Data Register */
#define MISO_opticalDriver_DR                     (* (reg8 *) MISO_opticalDriver__DR)
/* Port Number */
#define MISO_opticalDriver_PRT_NUM                (* (reg8 *) MISO_opticalDriver__PRT) 
/* Connect to Analog Globals */                                                  
#define MISO_opticalDriver_AG                     (* (reg8 *) MISO_opticalDriver__AG)                       
/* Analog MUX bux enable */
#define MISO_opticalDriver_AMUX                   (* (reg8 *) MISO_opticalDriver__AMUX) 
/* Bidirectional Enable */                                                        
#define MISO_opticalDriver_BIE                    (* (reg8 *) MISO_opticalDriver__BIE)
/* Bit-mask for Aliased Register Access */
#define MISO_opticalDriver_BIT_MASK               (* (reg8 *) MISO_opticalDriver__BIT_MASK)
/* Bypass Enable */
#define MISO_opticalDriver_BYP                    (* (reg8 *) MISO_opticalDriver__BYP)
/* Port wide control signals */                                                   
#define MISO_opticalDriver_CTL                    (* (reg8 *) MISO_opticalDriver__CTL)
/* Drive Modes */
#define MISO_opticalDriver_DM0                    (* (reg8 *) MISO_opticalDriver__DM0) 
#define MISO_opticalDriver_DM1                    (* (reg8 *) MISO_opticalDriver__DM1)
#define MISO_opticalDriver_DM2                    (* (reg8 *) MISO_opticalDriver__DM2) 
/* Input Buffer Disable Override */
#define MISO_opticalDriver_INP_DIS                (* (reg8 *) MISO_opticalDriver__INP_DIS)
/* LCD Common or Segment Drive */
#define MISO_opticalDriver_LCD_COM_SEG            (* (reg8 *) MISO_opticalDriver__LCD_COM_SEG)
/* Enable Segment LCD */
#define MISO_opticalDriver_LCD_EN                 (* (reg8 *) MISO_opticalDriver__LCD_EN)
/* Slew Rate Control */
#define MISO_opticalDriver_SLW                    (* (reg8 *) MISO_opticalDriver__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MISO_opticalDriver_PRTDSI__CAPS_SEL       (* (reg8 *) MISO_opticalDriver__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MISO_opticalDriver_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MISO_opticalDriver__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MISO_opticalDriver_PRTDSI__OE_SEL0        (* (reg8 *) MISO_opticalDriver__PRTDSI__OE_SEL0) 
#define MISO_opticalDriver_PRTDSI__OE_SEL1        (* (reg8 *) MISO_opticalDriver__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MISO_opticalDriver_PRTDSI__OUT_SEL0       (* (reg8 *) MISO_opticalDriver__PRTDSI__OUT_SEL0) 
#define MISO_opticalDriver_PRTDSI__OUT_SEL1       (* (reg8 *) MISO_opticalDriver__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MISO_opticalDriver_PRTDSI__SYNC_OUT       (* (reg8 *) MISO_opticalDriver__PRTDSI__SYNC_OUT) 


#if defined(MISO_opticalDriver__INTSTAT)  /* Interrupt Registers */

    #define MISO_opticalDriver_INTSTAT                (* (reg8 *) MISO_opticalDriver__INTSTAT)
    #define MISO_opticalDriver_SNAP                   (* (reg8 *) MISO_opticalDriver__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins MISO_opticalDriver_H */


/* [] END OF FILE */
