/*******************************************************************************
* File Name: NCS_opticalDirver.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NCS_opticalDirver_H) /* Pins NCS_opticalDirver_H */
#define CY_PINS_NCS_opticalDirver_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NCS_opticalDirver_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NCS_opticalDirver_Write(uint8 value) ;
void    NCS_opticalDirver_SetDriveMode(uint8 mode) ;
uint8   NCS_opticalDirver_ReadDataReg(void) ;
uint8   NCS_opticalDirver_Read(void) ;
uint8   NCS_opticalDirver_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NCS_opticalDirver_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NCS_opticalDirver_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NCS_opticalDirver_DM_RES_UP          PIN_DM_RES_UP
#define NCS_opticalDirver_DM_RES_DWN         PIN_DM_RES_DWN
#define NCS_opticalDirver_DM_OD_LO           PIN_DM_OD_LO
#define NCS_opticalDirver_DM_OD_HI           PIN_DM_OD_HI
#define NCS_opticalDirver_DM_STRONG          PIN_DM_STRONG
#define NCS_opticalDirver_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NCS_opticalDirver_MASK               NCS_opticalDirver__MASK
#define NCS_opticalDirver_SHIFT              NCS_opticalDirver__SHIFT
#define NCS_opticalDirver_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NCS_opticalDirver_PS                     (* (reg8 *) NCS_opticalDirver__PS)
/* Data Register */
#define NCS_opticalDirver_DR                     (* (reg8 *) NCS_opticalDirver__DR)
/* Port Number */
#define NCS_opticalDirver_PRT_NUM                (* (reg8 *) NCS_opticalDirver__PRT) 
/* Connect to Analog Globals */                                                  
#define NCS_opticalDirver_AG                     (* (reg8 *) NCS_opticalDirver__AG)                       
/* Analog MUX bux enable */
#define NCS_opticalDirver_AMUX                   (* (reg8 *) NCS_opticalDirver__AMUX) 
/* Bidirectional Enable */                                                        
#define NCS_opticalDirver_BIE                    (* (reg8 *) NCS_opticalDirver__BIE)
/* Bit-mask for Aliased Register Access */
#define NCS_opticalDirver_BIT_MASK               (* (reg8 *) NCS_opticalDirver__BIT_MASK)
/* Bypass Enable */
#define NCS_opticalDirver_BYP                    (* (reg8 *) NCS_opticalDirver__BYP)
/* Port wide control signals */                                                   
#define NCS_opticalDirver_CTL                    (* (reg8 *) NCS_opticalDirver__CTL)
/* Drive Modes */
#define NCS_opticalDirver_DM0                    (* (reg8 *) NCS_opticalDirver__DM0) 
#define NCS_opticalDirver_DM1                    (* (reg8 *) NCS_opticalDirver__DM1)
#define NCS_opticalDirver_DM2                    (* (reg8 *) NCS_opticalDirver__DM2) 
/* Input Buffer Disable Override */
#define NCS_opticalDirver_INP_DIS                (* (reg8 *) NCS_opticalDirver__INP_DIS)
/* LCD Common or Segment Drive */
#define NCS_opticalDirver_LCD_COM_SEG            (* (reg8 *) NCS_opticalDirver__LCD_COM_SEG)
/* Enable Segment LCD */
#define NCS_opticalDirver_LCD_EN                 (* (reg8 *) NCS_opticalDirver__LCD_EN)
/* Slew Rate Control */
#define NCS_opticalDirver_SLW                    (* (reg8 *) NCS_opticalDirver__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NCS_opticalDirver_PRTDSI__CAPS_SEL       (* (reg8 *) NCS_opticalDirver__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NCS_opticalDirver_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NCS_opticalDirver__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NCS_opticalDirver_PRTDSI__OE_SEL0        (* (reg8 *) NCS_opticalDirver__PRTDSI__OE_SEL0) 
#define NCS_opticalDirver_PRTDSI__OE_SEL1        (* (reg8 *) NCS_opticalDirver__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NCS_opticalDirver_PRTDSI__OUT_SEL0       (* (reg8 *) NCS_opticalDirver__PRTDSI__OUT_SEL0) 
#define NCS_opticalDirver_PRTDSI__OUT_SEL1       (* (reg8 *) NCS_opticalDirver__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NCS_opticalDirver_PRTDSI__SYNC_OUT       (* (reg8 *) NCS_opticalDirver__PRTDSI__SYNC_OUT) 


#if defined(NCS_opticalDirver__INTSTAT)  /* Interrupt Registers */

    #define NCS_opticalDirver_INTSTAT                (* (reg8 *) NCS_opticalDirver__INTSTAT)
    #define NCS_opticalDirver_SNAP                   (* (reg8 *) NCS_opticalDirver__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NCS_opticalDirver_H */


/* [] END OF FILE */
