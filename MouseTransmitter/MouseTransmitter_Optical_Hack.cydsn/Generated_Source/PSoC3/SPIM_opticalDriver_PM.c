/*******************************************************************************
* File Name: SPIM_opticalDriver_PM.c
* Version 2.40
*
* Description:
*  This file contains the setup, control and status commands to support
*  component operations in low power mode.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "SPIM_opticalDriver_PVT.h"

static SPIM_opticalDriver_BACKUP_STRUCT SPIM_opticalDriver_backup =
{
    SPIM_opticalDriver_DISABLED,
    SPIM_opticalDriver_BITCTR_INIT,
    #if(CY_UDB_V0)
        SPIM_opticalDriver_TX_INIT_INTERRUPTS_MASK,
        SPIM_opticalDriver_RX_INIT_INTERRUPTS_MASK
    #endif /* CY_UDB_V0 */
};


/*******************************************************************************
* Function Name: SPIM_opticalDriver_SaveConfig
********************************************************************************
*
* Summary:
*  Saves SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_opticalDriver_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_opticalDriver_SaveConfig(void) 
{
    /* Store Status Mask registers */
    #if(CY_UDB_V0)
       SPIM_opticalDriver_backup.cntrPeriod      = SPIM_opticalDriver_COUNTER_PERIOD_REG;
       SPIM_opticalDriver_backup.saveSrTxIntMask = SPIM_opticalDriver_TX_STATUS_MASK_REG;
       SPIM_opticalDriver_backup.saveSrRxIntMask = SPIM_opticalDriver_RX_STATUS_MASK_REG;
    #endif /* (CY_UDB_V0) */
}


/*******************************************************************************
* Function Name: SPIM_opticalDriver_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_opticalDriver_backup - used when non-retention registers are restored.
*
* Side Effects:
*  If this API is called without first calling SaveConfig then in the following
*  registers will be default values from Customizer:
*  SPIM_opticalDriver_STATUS_MASK_REG and SPIM_opticalDriver_COUNTER_PERIOD_REG.
*
*******************************************************************************/
void SPIM_opticalDriver_RestoreConfig(void) 
{
    /* Restore the data, saved by SaveConfig() function */
    #if(CY_UDB_V0)
        SPIM_opticalDriver_COUNTER_PERIOD_REG = SPIM_opticalDriver_backup.cntrPeriod;
        SPIM_opticalDriver_TX_STATUS_MASK_REG = ((uint8) SPIM_opticalDriver_backup.saveSrTxIntMask);
        SPIM_opticalDriver_RX_STATUS_MASK_REG = ((uint8) SPIM_opticalDriver_backup.saveSrRxIntMask);
    #endif /* (CY_UDB_V0) */
}


/*******************************************************************************
* Function Name: SPIM_opticalDriver_Sleep
********************************************************************************
*
* Summary:
*  Prepare SPIM Component goes to sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_opticalDriver_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_opticalDriver_Sleep(void) 
{
    /* Save components enable state */
    SPIM_opticalDriver_backup.enableState = ((uint8) SPIM_opticalDriver_IS_ENABLED);

    SPIM_opticalDriver_Stop();
    SPIM_opticalDriver_SaveConfig();
}


/*******************************************************************************
* Function Name: SPIM_opticalDriver_Wakeup
********************************************************************************
*
* Summary:
*  Prepare SPIM Component to wake up.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  SPIM_opticalDriver_backup - used when non-retention registers are restored.
*  SPIM_opticalDriver_txBufferWrite - modified every function call - resets to
*  zero.
*  SPIM_opticalDriver_txBufferRead - modified every function call - resets to
*  zero.
*  SPIM_opticalDriver_rxBufferWrite - modified every function call - resets to
*  zero.
*  SPIM_opticalDriver_rxBufferRead - modified every function call - resets to
*  zero.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void SPIM_opticalDriver_Wakeup(void) 
{
    SPIM_opticalDriver_RestoreConfig();

    #if(SPIM_opticalDriver_RX_SOFTWARE_BUF_ENABLED)
        SPIM_opticalDriver_rxBufferFull  = 0u;
        SPIM_opticalDriver_rxBufferRead  = 0u;
        SPIM_opticalDriver_rxBufferWrite = 0u;
    #endif /* (SPIM_opticalDriver_RX_SOFTWARE_BUF_ENABLED) */

    #if(SPIM_opticalDriver_TX_SOFTWARE_BUF_ENABLED)
        SPIM_opticalDriver_txBufferFull  = 0u;
        SPIM_opticalDriver_txBufferRead  = 0u;
        SPIM_opticalDriver_txBufferWrite = 0u;
    #endif /* (SPIM_opticalDriver_TX_SOFTWARE_BUF_ENABLED) */

    /* Clear any data from the RX and TX FIFO */
    SPIM_opticalDriver_ClearFIFO();

    /* Restore components block enable state */
    if(0u != SPIM_opticalDriver_backup.enableState)
    {
        SPIM_opticalDriver_Enable();
    }
}


/* [] END OF FILE */
