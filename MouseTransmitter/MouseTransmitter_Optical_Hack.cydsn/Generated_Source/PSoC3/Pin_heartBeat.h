/*******************************************************************************
* File Name: Pin_heartBeat.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_heartBeat_H) /* Pins Pin_heartBeat_H */
#define CY_PINS_Pin_heartBeat_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_heartBeat_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_heartBeat_Write(uint8 value) ;
void    Pin_heartBeat_SetDriveMode(uint8 mode) ;
uint8   Pin_heartBeat_ReadDataReg(void) ;
uint8   Pin_heartBeat_Read(void) ;
uint8   Pin_heartBeat_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_heartBeat_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_heartBeat_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_heartBeat_DM_RES_UP          PIN_DM_RES_UP
#define Pin_heartBeat_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_heartBeat_DM_OD_LO           PIN_DM_OD_LO
#define Pin_heartBeat_DM_OD_HI           PIN_DM_OD_HI
#define Pin_heartBeat_DM_STRONG          PIN_DM_STRONG
#define Pin_heartBeat_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_heartBeat_MASK               Pin_heartBeat__MASK
#define Pin_heartBeat_SHIFT              Pin_heartBeat__SHIFT
#define Pin_heartBeat_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_heartBeat_PS                     (* (reg8 *) Pin_heartBeat__PS)
/* Data Register */
#define Pin_heartBeat_DR                     (* (reg8 *) Pin_heartBeat__DR)
/* Port Number */
#define Pin_heartBeat_PRT_NUM                (* (reg8 *) Pin_heartBeat__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_heartBeat_AG                     (* (reg8 *) Pin_heartBeat__AG)                       
/* Analog MUX bux enable */
#define Pin_heartBeat_AMUX                   (* (reg8 *) Pin_heartBeat__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_heartBeat_BIE                    (* (reg8 *) Pin_heartBeat__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_heartBeat_BIT_MASK               (* (reg8 *) Pin_heartBeat__BIT_MASK)
/* Bypass Enable */
#define Pin_heartBeat_BYP                    (* (reg8 *) Pin_heartBeat__BYP)
/* Port wide control signals */                                                   
#define Pin_heartBeat_CTL                    (* (reg8 *) Pin_heartBeat__CTL)
/* Drive Modes */
#define Pin_heartBeat_DM0                    (* (reg8 *) Pin_heartBeat__DM0) 
#define Pin_heartBeat_DM1                    (* (reg8 *) Pin_heartBeat__DM1)
#define Pin_heartBeat_DM2                    (* (reg8 *) Pin_heartBeat__DM2) 
/* Input Buffer Disable Override */
#define Pin_heartBeat_INP_DIS                (* (reg8 *) Pin_heartBeat__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_heartBeat_LCD_COM_SEG            (* (reg8 *) Pin_heartBeat__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_heartBeat_LCD_EN                 (* (reg8 *) Pin_heartBeat__LCD_EN)
/* Slew Rate Control */
#define Pin_heartBeat_SLW                    (* (reg8 *) Pin_heartBeat__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_heartBeat_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_heartBeat__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_heartBeat_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_heartBeat__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_heartBeat_PRTDSI__OE_SEL0        (* (reg8 *) Pin_heartBeat__PRTDSI__OE_SEL0) 
#define Pin_heartBeat_PRTDSI__OE_SEL1        (* (reg8 *) Pin_heartBeat__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_heartBeat_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_heartBeat__PRTDSI__OUT_SEL0) 
#define Pin_heartBeat_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_heartBeat__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_heartBeat_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_heartBeat__PRTDSI__SYNC_OUT) 


#if defined(Pin_heartBeat__INTSTAT)  /* Interrupt Registers */

    #define Pin_heartBeat_INTSTAT                (* (reg8 *) Pin_heartBeat__INTSTAT)
    #define Pin_heartBeat_SNAP                   (* (reg8 *) Pin_heartBeat__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Pin_heartBeat_H */


/* [] END OF FILE */
