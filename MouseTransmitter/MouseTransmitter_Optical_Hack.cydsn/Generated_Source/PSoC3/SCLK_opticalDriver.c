/*******************************************************************************
* File Name: SCLK_opticalDriver.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "SCLK_opticalDriver.h"


/*******************************************************************************
* Function Name: SCLK_opticalDriver_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void SCLK_opticalDriver_Write(uint8 value) 
{
    uint8 staticBits = (SCLK_opticalDriver_DR & (uint8)(~SCLK_opticalDriver_MASK));
    SCLK_opticalDriver_DR = staticBits | ((uint8)(value << SCLK_opticalDriver_SHIFT) & SCLK_opticalDriver_MASK);
}


/*******************************************************************************
* Function Name: SCLK_opticalDriver_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void SCLK_opticalDriver_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(SCLK_opticalDriver_0, mode);
}


/*******************************************************************************
* Function Name: SCLK_opticalDriver_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro SCLK_opticalDriver_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 SCLK_opticalDriver_Read(void) 
{
    return (SCLK_opticalDriver_PS & SCLK_opticalDriver_MASK) >> SCLK_opticalDriver_SHIFT;
}


/*******************************************************************************
* Function Name: SCLK_opticalDriver_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 SCLK_opticalDriver_ReadDataReg(void) 
{
    return (SCLK_opticalDriver_DR & SCLK_opticalDriver_MASK) >> SCLK_opticalDriver_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(SCLK_opticalDriver_INTSTAT) 

    /*******************************************************************************
    * Function Name: SCLK_opticalDriver_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 SCLK_opticalDriver_ClearInterrupt(void) 
    {
        return (SCLK_opticalDriver_INTSTAT & SCLK_opticalDriver_MASK) >> SCLK_opticalDriver_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
