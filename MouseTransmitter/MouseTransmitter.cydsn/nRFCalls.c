/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "nRF24L01.h"
#include "const.h"



void blink(uint8 repetition)
{
    switch(repetition){
        case 0: PWM_heartBeat_WritePeriod(40);
                PWM_heartBeat_WriteCompare(20);           
        break;
        
                case 1:PWM_heartBeat_WritePeriod(150);
                PWM_heartBeat_WriteCompare(75);
        break;
        
                case 2:PWM_heartBeat_WritePeriod(255);
                PWM_heartBeat_WriteCompare(127);
        break;
                
                case 3:PWM_heartBeat_WritePeriod(20);
                PWM_heartBeat_WriteCompare(10);
        break;
        
        default:PWM_heartBeat_WritePeriod(50);
                PWM_heartBeat_WriteCompare(25);
        break;
    }
}

int TX_PL_Write( uint8 *Data, int Data_Len){   // can write up to 32 Bytes to the TX FIFO of the transmitter
  char temp;
  int j;
  CSN_LOW();
  SPI_SendByte(W_TX_PAYLOAD);
  for (j=0;j<(Data_Len);j++){
    SPI_SendByte(*(Data+j));
    temp = *(Data+j);
  }
  CSN_HIGH();
  return ERROR_NO_ERROR;
}


int transmitData(uint8 *Data, int Data_Len)
    {
        uint8 status;
        uint8 status_temp;
        
        TX_PL_Write((uint8*)Data,Data_Len);
        
        CE_HIGH();
        CE_LOW();
    
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        while ((status  & TX_DS) != TX_DS )
        {
            if ((status & MAX_RT) != 0) {
            // Clear MAX_RT bit in status register
            status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
            // LCD_Position(0,0);
            // LCD_PrintString("Packet lost");//Toggle LED1 to indicate that a packet is lost during Transmission
            blink(1);
            // No communication event here
            // ....
            status = SPI_Send_command_without_ADDR(NOP,NOP);
            return(ERROR);
            }
            status = SPI_Send_command_without_ADDR(NOP,NOP);
        }

        if ((status & TX_DS) != 0) 
        {
            //One packet of data sent successfully
            // Clear TX_DS bit in status register
            status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|TX_DS));
            // LCD_Position(0,0);
            // LCD_PrintString("1 Packet sent");
            blink(0);
            return(ERROR_NO_ERROR);
        }
        
        if ((status & MAX_RT) != 0) 
        {
            //Maximum Retries interrupt set
            //Clear MAX_RT bit in status register
            status_temp = SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status|MAX_RT));
//          LCD_Position(0,0);
//          LCD_PrintString("MAX_RT is cleared");
            blink(3);
            return(ERROR);
        }

        if ((status & TX_FULL) != 0)  {
          // Flush TX FIFO (in TX mode          
          status_temp = SPI_Send_command_without_ADDR(FLUSH_TX, NOP);
        }
        status = SPI_Send_command_without_ADDR(NOP,NOP);
        return(ERROR_NO_ERROR);
    }
    

    

/* [] END OF FILE */
