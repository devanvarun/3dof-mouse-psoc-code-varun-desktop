/*******************************************************************************
* File Name: NRF_MISO.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_MISO_H) /* Pins NRF_MISO_H */
#define CY_PINS_NRF_MISO_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_MISO_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_MISO_Write(uint8 value) ;
void    NRF_MISO_SetDriveMode(uint8 mode) ;
uint8   NRF_MISO_ReadDataReg(void) ;
uint8   NRF_MISO_Read(void) ;
uint8   NRF_MISO_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_MISO_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_MISO_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_MISO_DM_RES_UP          PIN_DM_RES_UP
#define NRF_MISO_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_MISO_DM_OD_LO           PIN_DM_OD_LO
#define NRF_MISO_DM_OD_HI           PIN_DM_OD_HI
#define NRF_MISO_DM_STRONG          PIN_DM_STRONG
#define NRF_MISO_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_MISO_MASK               NRF_MISO__MASK
#define NRF_MISO_SHIFT              NRF_MISO__SHIFT
#define NRF_MISO_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_MISO_PS                     (* (reg8 *) NRF_MISO__PS)
/* Data Register */
#define NRF_MISO_DR                     (* (reg8 *) NRF_MISO__DR)
/* Port Number */
#define NRF_MISO_PRT_NUM                (* (reg8 *) NRF_MISO__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_MISO_AG                     (* (reg8 *) NRF_MISO__AG)                       
/* Analog MUX bux enable */
#define NRF_MISO_AMUX                   (* (reg8 *) NRF_MISO__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_MISO_BIE                    (* (reg8 *) NRF_MISO__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_MISO_BIT_MASK               (* (reg8 *) NRF_MISO__BIT_MASK)
/* Bypass Enable */
#define NRF_MISO_BYP                    (* (reg8 *) NRF_MISO__BYP)
/* Port wide control signals */                                                   
#define NRF_MISO_CTL                    (* (reg8 *) NRF_MISO__CTL)
/* Drive Modes */
#define NRF_MISO_DM0                    (* (reg8 *) NRF_MISO__DM0) 
#define NRF_MISO_DM1                    (* (reg8 *) NRF_MISO__DM1)
#define NRF_MISO_DM2                    (* (reg8 *) NRF_MISO__DM2) 
/* Input Buffer Disable Override */
#define NRF_MISO_INP_DIS                (* (reg8 *) NRF_MISO__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_MISO_LCD_COM_SEG            (* (reg8 *) NRF_MISO__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_MISO_LCD_EN                 (* (reg8 *) NRF_MISO__LCD_EN)
/* Slew Rate Control */
#define NRF_MISO_SLW                    (* (reg8 *) NRF_MISO__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_MISO_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_MISO__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_MISO_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_MISO__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_MISO_PRTDSI__OE_SEL0        (* (reg8 *) NRF_MISO__PRTDSI__OE_SEL0) 
#define NRF_MISO_PRTDSI__OE_SEL1        (* (reg8 *) NRF_MISO__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_MISO_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_MISO__PRTDSI__OUT_SEL0) 
#define NRF_MISO_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_MISO__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_MISO_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_MISO__PRTDSI__SYNC_OUT) 


#if defined(NRF_MISO__INTSTAT)  /* Interrupt Registers */

    #define NRF_MISO_INTSTAT                (* (reg8 *) NRF_MISO__INTSTAT)
    #define NRF_MISO_SNAP                   (* (reg8 *) NRF_MISO__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_MISO_H */


/* [] END OF FILE */
