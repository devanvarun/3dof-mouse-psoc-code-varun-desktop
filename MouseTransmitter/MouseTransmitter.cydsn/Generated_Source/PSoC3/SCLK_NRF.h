/*******************************************************************************
* File Name: SCLK_NRF.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SCLK_NRF_H) /* Pins SCLK_NRF_H */
#define CY_PINS_SCLK_NRF_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "SCLK_NRF_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    SCLK_NRF_Write(uint8 value) ;
void    SCLK_NRF_SetDriveMode(uint8 mode) ;
uint8   SCLK_NRF_ReadDataReg(void) ;
uint8   SCLK_NRF_Read(void) ;
uint8   SCLK_NRF_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define SCLK_NRF_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define SCLK_NRF_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define SCLK_NRF_DM_RES_UP          PIN_DM_RES_UP
#define SCLK_NRF_DM_RES_DWN         PIN_DM_RES_DWN
#define SCLK_NRF_DM_OD_LO           PIN_DM_OD_LO
#define SCLK_NRF_DM_OD_HI           PIN_DM_OD_HI
#define SCLK_NRF_DM_STRONG          PIN_DM_STRONG
#define SCLK_NRF_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define SCLK_NRF_MASK               SCLK_NRF__MASK
#define SCLK_NRF_SHIFT              SCLK_NRF__SHIFT
#define SCLK_NRF_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SCLK_NRF_PS                     (* (reg8 *) SCLK_NRF__PS)
/* Data Register */
#define SCLK_NRF_DR                     (* (reg8 *) SCLK_NRF__DR)
/* Port Number */
#define SCLK_NRF_PRT_NUM                (* (reg8 *) SCLK_NRF__PRT) 
/* Connect to Analog Globals */                                                  
#define SCLK_NRF_AG                     (* (reg8 *) SCLK_NRF__AG)                       
/* Analog MUX bux enable */
#define SCLK_NRF_AMUX                   (* (reg8 *) SCLK_NRF__AMUX) 
/* Bidirectional Enable */                                                        
#define SCLK_NRF_BIE                    (* (reg8 *) SCLK_NRF__BIE)
/* Bit-mask for Aliased Register Access */
#define SCLK_NRF_BIT_MASK               (* (reg8 *) SCLK_NRF__BIT_MASK)
/* Bypass Enable */
#define SCLK_NRF_BYP                    (* (reg8 *) SCLK_NRF__BYP)
/* Port wide control signals */                                                   
#define SCLK_NRF_CTL                    (* (reg8 *) SCLK_NRF__CTL)
/* Drive Modes */
#define SCLK_NRF_DM0                    (* (reg8 *) SCLK_NRF__DM0) 
#define SCLK_NRF_DM1                    (* (reg8 *) SCLK_NRF__DM1)
#define SCLK_NRF_DM2                    (* (reg8 *) SCLK_NRF__DM2) 
/* Input Buffer Disable Override */
#define SCLK_NRF_INP_DIS                (* (reg8 *) SCLK_NRF__INP_DIS)
/* LCD Common or Segment Drive */
#define SCLK_NRF_LCD_COM_SEG            (* (reg8 *) SCLK_NRF__LCD_COM_SEG)
/* Enable Segment LCD */
#define SCLK_NRF_LCD_EN                 (* (reg8 *) SCLK_NRF__LCD_EN)
/* Slew Rate Control */
#define SCLK_NRF_SLW                    (* (reg8 *) SCLK_NRF__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define SCLK_NRF_PRTDSI__CAPS_SEL       (* (reg8 *) SCLK_NRF__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define SCLK_NRF_PRTDSI__DBL_SYNC_IN    (* (reg8 *) SCLK_NRF__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define SCLK_NRF_PRTDSI__OE_SEL0        (* (reg8 *) SCLK_NRF__PRTDSI__OE_SEL0) 
#define SCLK_NRF_PRTDSI__OE_SEL1        (* (reg8 *) SCLK_NRF__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define SCLK_NRF_PRTDSI__OUT_SEL0       (* (reg8 *) SCLK_NRF__PRTDSI__OUT_SEL0) 
#define SCLK_NRF_PRTDSI__OUT_SEL1       (* (reg8 *) SCLK_NRF__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define SCLK_NRF_PRTDSI__SYNC_OUT       (* (reg8 *) SCLK_NRF__PRTDSI__SYNC_OUT) 


#if defined(SCLK_NRF__INTSTAT)  /* Interrupt Registers */

    #define SCLK_NRF_INTSTAT                (* (reg8 *) SCLK_NRF__INTSTAT)
    #define SCLK_NRF_SNAP                   (* (reg8 *) SCLK_NRF__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins SCLK_NRF_H */


/* [] END OF FILE */
