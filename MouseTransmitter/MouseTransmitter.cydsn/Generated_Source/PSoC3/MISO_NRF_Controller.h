/*******************************************************************************
* File Name: MISO_NRF_Controller.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_MISO_NRF_Controller_H) /* Pins MISO_NRF_Controller_H */
#define CY_PINS_MISO_NRF_Controller_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "MISO_NRF_Controller_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    MISO_NRF_Controller_Write(uint8 value) ;
void    MISO_NRF_Controller_SetDriveMode(uint8 mode) ;
uint8   MISO_NRF_Controller_ReadDataReg(void) ;
uint8   MISO_NRF_Controller_Read(void) ;
uint8   MISO_NRF_Controller_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define MISO_NRF_Controller_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define MISO_NRF_Controller_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define MISO_NRF_Controller_DM_RES_UP          PIN_DM_RES_UP
#define MISO_NRF_Controller_DM_RES_DWN         PIN_DM_RES_DWN
#define MISO_NRF_Controller_DM_OD_LO           PIN_DM_OD_LO
#define MISO_NRF_Controller_DM_OD_HI           PIN_DM_OD_HI
#define MISO_NRF_Controller_DM_STRONG          PIN_DM_STRONG
#define MISO_NRF_Controller_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define MISO_NRF_Controller_MASK               MISO_NRF_Controller__MASK
#define MISO_NRF_Controller_SHIFT              MISO_NRF_Controller__SHIFT
#define MISO_NRF_Controller_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define MISO_NRF_Controller_PS                     (* (reg8 *) MISO_NRF_Controller__PS)
/* Data Register */
#define MISO_NRF_Controller_DR                     (* (reg8 *) MISO_NRF_Controller__DR)
/* Port Number */
#define MISO_NRF_Controller_PRT_NUM                (* (reg8 *) MISO_NRF_Controller__PRT) 
/* Connect to Analog Globals */                                                  
#define MISO_NRF_Controller_AG                     (* (reg8 *) MISO_NRF_Controller__AG)                       
/* Analog MUX bux enable */
#define MISO_NRF_Controller_AMUX                   (* (reg8 *) MISO_NRF_Controller__AMUX) 
/* Bidirectional Enable */                                                        
#define MISO_NRF_Controller_BIE                    (* (reg8 *) MISO_NRF_Controller__BIE)
/* Bit-mask for Aliased Register Access */
#define MISO_NRF_Controller_BIT_MASK               (* (reg8 *) MISO_NRF_Controller__BIT_MASK)
/* Bypass Enable */
#define MISO_NRF_Controller_BYP                    (* (reg8 *) MISO_NRF_Controller__BYP)
/* Port wide control signals */                                                   
#define MISO_NRF_Controller_CTL                    (* (reg8 *) MISO_NRF_Controller__CTL)
/* Drive Modes */
#define MISO_NRF_Controller_DM0                    (* (reg8 *) MISO_NRF_Controller__DM0) 
#define MISO_NRF_Controller_DM1                    (* (reg8 *) MISO_NRF_Controller__DM1)
#define MISO_NRF_Controller_DM2                    (* (reg8 *) MISO_NRF_Controller__DM2) 
/* Input Buffer Disable Override */
#define MISO_NRF_Controller_INP_DIS                (* (reg8 *) MISO_NRF_Controller__INP_DIS)
/* LCD Common or Segment Drive */
#define MISO_NRF_Controller_LCD_COM_SEG            (* (reg8 *) MISO_NRF_Controller__LCD_COM_SEG)
/* Enable Segment LCD */
#define MISO_NRF_Controller_LCD_EN                 (* (reg8 *) MISO_NRF_Controller__LCD_EN)
/* Slew Rate Control */
#define MISO_NRF_Controller_SLW                    (* (reg8 *) MISO_NRF_Controller__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define MISO_NRF_Controller_PRTDSI__CAPS_SEL       (* (reg8 *) MISO_NRF_Controller__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define MISO_NRF_Controller_PRTDSI__DBL_SYNC_IN    (* (reg8 *) MISO_NRF_Controller__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define MISO_NRF_Controller_PRTDSI__OE_SEL0        (* (reg8 *) MISO_NRF_Controller__PRTDSI__OE_SEL0) 
#define MISO_NRF_Controller_PRTDSI__OE_SEL1        (* (reg8 *) MISO_NRF_Controller__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define MISO_NRF_Controller_PRTDSI__OUT_SEL0       (* (reg8 *) MISO_NRF_Controller__PRTDSI__OUT_SEL0) 
#define MISO_NRF_Controller_PRTDSI__OUT_SEL1       (* (reg8 *) MISO_NRF_Controller__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define MISO_NRF_Controller_PRTDSI__SYNC_OUT       (* (reg8 *) MISO_NRF_Controller__PRTDSI__SYNC_OUT) 


#if defined(MISO_NRF_Controller__INTSTAT)  /* Interrupt Registers */

    #define MISO_NRF_Controller_INTSTAT                (* (reg8 *) MISO_NRF_Controller__INTSTAT)
    #define MISO_NRF_Controller_SNAP                   (* (reg8 *) MISO_NRF_Controller__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins MISO_NRF_Controller_H */


/* [] END OF FILE */
