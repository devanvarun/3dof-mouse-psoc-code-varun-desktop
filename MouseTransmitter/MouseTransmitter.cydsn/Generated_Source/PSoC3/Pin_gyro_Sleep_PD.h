/*******************************************************************************
* File Name: Pin_gyro_Sleep_PD.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_gyro_Sleep_PD_H) /* Pins Pin_gyro_Sleep_PD_H */
#define CY_PINS_Pin_gyro_Sleep_PD_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_gyro_Sleep_PD_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_gyro_Sleep_PD_Write(uint8 value) ;
void    Pin_gyro_Sleep_PD_SetDriveMode(uint8 mode) ;
uint8   Pin_gyro_Sleep_PD_ReadDataReg(void) ;
uint8   Pin_gyro_Sleep_PD_Read(void) ;
uint8   Pin_gyro_Sleep_PD_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_gyro_Sleep_PD_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_gyro_Sleep_PD_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_gyro_Sleep_PD_DM_RES_UP          PIN_DM_RES_UP
#define Pin_gyro_Sleep_PD_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_gyro_Sleep_PD_DM_OD_LO           PIN_DM_OD_LO
#define Pin_gyro_Sleep_PD_DM_OD_HI           PIN_DM_OD_HI
#define Pin_gyro_Sleep_PD_DM_STRONG          PIN_DM_STRONG
#define Pin_gyro_Sleep_PD_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_gyro_Sleep_PD_MASK               Pin_gyro_Sleep_PD__MASK
#define Pin_gyro_Sleep_PD_SHIFT              Pin_gyro_Sleep_PD__SHIFT
#define Pin_gyro_Sleep_PD_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_gyro_Sleep_PD_PS                     (* (reg8 *) Pin_gyro_Sleep_PD__PS)
/* Data Register */
#define Pin_gyro_Sleep_PD_DR                     (* (reg8 *) Pin_gyro_Sleep_PD__DR)
/* Port Number */
#define Pin_gyro_Sleep_PD_PRT_NUM                (* (reg8 *) Pin_gyro_Sleep_PD__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_gyro_Sleep_PD_AG                     (* (reg8 *) Pin_gyro_Sleep_PD__AG)                       
/* Analog MUX bux enable */
#define Pin_gyro_Sleep_PD_AMUX                   (* (reg8 *) Pin_gyro_Sleep_PD__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_gyro_Sleep_PD_BIE                    (* (reg8 *) Pin_gyro_Sleep_PD__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_gyro_Sleep_PD_BIT_MASK               (* (reg8 *) Pin_gyro_Sleep_PD__BIT_MASK)
/* Bypass Enable */
#define Pin_gyro_Sleep_PD_BYP                    (* (reg8 *) Pin_gyro_Sleep_PD__BYP)
/* Port wide control signals */                                                   
#define Pin_gyro_Sleep_PD_CTL                    (* (reg8 *) Pin_gyro_Sleep_PD__CTL)
/* Drive Modes */
#define Pin_gyro_Sleep_PD_DM0                    (* (reg8 *) Pin_gyro_Sleep_PD__DM0) 
#define Pin_gyro_Sleep_PD_DM1                    (* (reg8 *) Pin_gyro_Sleep_PD__DM1)
#define Pin_gyro_Sleep_PD_DM2                    (* (reg8 *) Pin_gyro_Sleep_PD__DM2) 
/* Input Buffer Disable Override */
#define Pin_gyro_Sleep_PD_INP_DIS                (* (reg8 *) Pin_gyro_Sleep_PD__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_gyro_Sleep_PD_LCD_COM_SEG            (* (reg8 *) Pin_gyro_Sleep_PD__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_gyro_Sleep_PD_LCD_EN                 (* (reg8 *) Pin_gyro_Sleep_PD__LCD_EN)
/* Slew Rate Control */
#define Pin_gyro_Sleep_PD_SLW                    (* (reg8 *) Pin_gyro_Sleep_PD__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_gyro_Sleep_PD_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_gyro_Sleep_PD_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_gyro_Sleep_PD_PRTDSI__OE_SEL0        (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__OE_SEL0) 
#define Pin_gyro_Sleep_PD_PRTDSI__OE_SEL1        (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_gyro_Sleep_PD_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__OUT_SEL0) 
#define Pin_gyro_Sleep_PD_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_gyro_Sleep_PD_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_gyro_Sleep_PD__PRTDSI__SYNC_OUT) 


#if defined(Pin_gyro_Sleep_PD__INTSTAT)  /* Interrupt Registers */

    #define Pin_gyro_Sleep_PD_INTSTAT                (* (reg8 *) Pin_gyro_Sleep_PD__INTSTAT)
    #define Pin_gyro_Sleep_PD_SNAP                   (* (reg8 *) Pin_gyro_Sleep_PD__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Pin_gyro_Sleep_PD_H */


/* [] END OF FILE */
