/*******************************************************************************
* File Name: CE_NRF.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CE_NRF_ALIASES_H) /* Pins CE_NRF_ALIASES_H */
#define CY_PINS_CE_NRF_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define CE_NRF_0		CE_NRF__0__PC

#endif /* End Pins CE_NRF_ALIASES_H */


/* [] END OF FILE */
