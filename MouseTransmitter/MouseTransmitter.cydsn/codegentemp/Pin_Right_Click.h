/*******************************************************************************
* File Name: Pin_Right_Click.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_Right_Click_H) /* Pins Pin_Right_Click_H */
#define CY_PINS_Pin_Right_Click_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_Right_Click_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_Right_Click_Write(uint8 value) ;
void    Pin_Right_Click_SetDriveMode(uint8 mode) ;
uint8   Pin_Right_Click_ReadDataReg(void) ;
uint8   Pin_Right_Click_Read(void) ;
uint8   Pin_Right_Click_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_Right_Click_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_Right_Click_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_Right_Click_DM_RES_UP          PIN_DM_RES_UP
#define Pin_Right_Click_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_Right_Click_DM_OD_LO           PIN_DM_OD_LO
#define Pin_Right_Click_DM_OD_HI           PIN_DM_OD_HI
#define Pin_Right_Click_DM_STRONG          PIN_DM_STRONG
#define Pin_Right_Click_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_Right_Click_MASK               Pin_Right_Click__MASK
#define Pin_Right_Click_SHIFT              Pin_Right_Click__SHIFT
#define Pin_Right_Click_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_Right_Click_PS                     (* (reg8 *) Pin_Right_Click__PS)
/* Data Register */
#define Pin_Right_Click_DR                     (* (reg8 *) Pin_Right_Click__DR)
/* Port Number */
#define Pin_Right_Click_PRT_NUM                (* (reg8 *) Pin_Right_Click__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_Right_Click_AG                     (* (reg8 *) Pin_Right_Click__AG)                       
/* Analog MUX bux enable */
#define Pin_Right_Click_AMUX                   (* (reg8 *) Pin_Right_Click__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_Right_Click_BIE                    (* (reg8 *) Pin_Right_Click__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_Right_Click_BIT_MASK               (* (reg8 *) Pin_Right_Click__BIT_MASK)
/* Bypass Enable */
#define Pin_Right_Click_BYP                    (* (reg8 *) Pin_Right_Click__BYP)
/* Port wide control signals */                                                   
#define Pin_Right_Click_CTL                    (* (reg8 *) Pin_Right_Click__CTL)
/* Drive Modes */
#define Pin_Right_Click_DM0                    (* (reg8 *) Pin_Right_Click__DM0) 
#define Pin_Right_Click_DM1                    (* (reg8 *) Pin_Right_Click__DM1)
#define Pin_Right_Click_DM2                    (* (reg8 *) Pin_Right_Click__DM2) 
/* Input Buffer Disable Override */
#define Pin_Right_Click_INP_DIS                (* (reg8 *) Pin_Right_Click__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_Right_Click_LCD_COM_SEG            (* (reg8 *) Pin_Right_Click__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_Right_Click_LCD_EN                 (* (reg8 *) Pin_Right_Click__LCD_EN)
/* Slew Rate Control */
#define Pin_Right_Click_SLW                    (* (reg8 *) Pin_Right_Click__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_Right_Click_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_Right_Click__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_Right_Click_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_Right_Click__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_Right_Click_PRTDSI__OE_SEL0        (* (reg8 *) Pin_Right_Click__PRTDSI__OE_SEL0) 
#define Pin_Right_Click_PRTDSI__OE_SEL1        (* (reg8 *) Pin_Right_Click__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_Right_Click_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_Right_Click__PRTDSI__OUT_SEL0) 
#define Pin_Right_Click_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_Right_Click__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_Right_Click_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_Right_Click__PRTDSI__SYNC_OUT) 


#if defined(Pin_Right_Click__INTSTAT)  /* Interrupt Registers */

    #define Pin_Right_Click_INTSTAT                (* (reg8 *) Pin_Right_Click__INTSTAT)
    #define Pin_Right_Click_SNAP                   (* (reg8 *) Pin_Right_Click__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Pin_Right_Click_H */


/* [] END OF FILE */
