/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "nRF24L01.h"
#include "const.h"

#define ERROR_NO_ERROR  0
#define MAXINT32 4294836225
#define USB_ENABLED 1

uint8 dataReceivedFlag = 0;
uint8 rxCount = 0;
int8 Mouse_Data[4] = {0, 0, 0, 0}; /* [0] = Buttons, [1] = X-Axis, [2] = Y-Axis [3]= ScrollWheel*/
uint8 Rx_Data[32];

int Rx_PL_Read( uint8 *Data, int Data_Len);
void getData(void);

uint8 myData[1024] ={0};
uint16 myDataCounter = 0;

int main()
{

    uint8 status;
    int i;
    uint32 datacounter1;
    uint32 datacounter2;
    
    CyGlobalIntEnable; 
    PWM_1_Start();
    
    isr_nrf_Start();
    LCD_Start();
    SPIM_NRF_Controller_Start();
    nRFSetting();
    CE_HIGH(); 
    //nRF_Test();
    LCD_Position(0,0);
    LCD_PrintNumber(status);
    
    #if(USB_ENABLED)
    USBFS_1_Start(0, USBFS_1_DWR_VDDD_OPERATION);   /* Start USBFS Operation/device 0 and with 5V operation */ 	
	while(!USBFS_1_bGetConfiguration());      		/* Wait for Device to enumerate */    
    USBFS_1_LoadInEP(1, (uint8 *)Mouse_Data, 4); 	/* Loads an inital value into EP1 and sends it out to the PC */
    #endif
    
    for(;;)
    {
        /* Place your application code here. */
    #if(USB_ENABLED)
        while(!USBFS_1_bGetEPAckState(1));  			/* Wait for ACK before loading data */
    //    if(USBFS_1_bGetEPAckState(1)){
        USBFS_1_LoadInEP(1, (uint8 *)Mouse_Data, 4); 	/* Load latest mouse data into EP1 and send to PC */
    //    }
     
    #endif
    
        if(dataReceivedFlag == 1)
        {
            getData();    
      
            datacounter1++;
            if((datacounter1)>MAXINT32)
            {
             datacounter2++;   
            }
        
        }
        
        else
        {
           // Mouse_Data[0] = 0;
            Mouse_Data[1] = 0;
            Mouse_Data[2] = 0;
            Mouse_Data[3] = 0;
        }
                
    }
}

void getData(void)
{
    int i;
    uint8 status = 0;
    
    dataReceivedFlag = 0;   
    NRF_IRQ_ClearInterrupt();
    
    status = SPI_Send_command_without_ADDR(NOP,NOP);
    if ((status  & RX_DR) == RX_DR )
    {
//     LCD_Position(0,0);
//     LCD_PrintString("Data Rcvd");
    SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status| RX_DR));
    CE_LOW();
    //SPI_Send_command_without_ADDR(CMD_RX_PL_WID,NOP);
    //rxCount = SPI_Send_command_without_ADDR(NOP,NOP);
    Rx_PL_Read(&Rx_Data[0],4);  
//     LCD_Position(1,0);
    for(i=0;i<4;i++){   
//         LCD_PrintNumber((int8)Rx_Data[i]);
//         LCD_PutChar(' ');
        Mouse_Data[i] = (int8)Rx_Data[i];
         
    }
    
    if(myDataCounter<1024)
    myData[myDataCounter++] = Mouse_Data[0]; 
    
    
    isr_nrf_ClearPending();
    CE_HIGH();
             
    }
}

int Rx_PL_Read( uint8 *Data, int Data_Len){
  int j;
  CSN_LOW();
  SPI_SendByte(R_RX_PAYLOAD);
  for (j=0;j<(Data_Len);j++){
    *(Data+j) = SPI_SendByte(NOP);
  }
  CSN_HIGH();
  return ERROR_NO_ERROR;
}

 
/* [] END OF FILE */
