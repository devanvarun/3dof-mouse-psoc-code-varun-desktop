

//CONSTANTS
#define   NOP             0xff
#define   R_REGISTER      0x00
#define   W_REGISTER      0x01
#define   CMD_RX_PL_WID   0x60
#define   R_RX_PAYLOAD    0x61
#define   W_TX_PAYLOAD    0xA0
#define   W_TX_PAYLOAD_NOACK 0xB0
#define   FLUSH_TX        0xE1
#define   FLUSH_RX        0xE2
#define   REUSE_TX_PL     0xE3
#define   FEATURE_ADDR    0x1D

#define   RX_ADDR_P0      0x0A
#define   RX_ADDR_P1      0x0B
#define   TX_ADDR         0x10
#define   RX_PW_P0        0x11
#define   RX_PW_P1        0x12
#define   FIFO_STATUS     0x17

#define   MAX_RT          0x10

#define   CONFIG_REG_ADDR 0x00
#define   STATUS_ADDR     0x07

#define   FLUSH_TX        0xE1
#define   TX_FULL         0x01
#define   RX_DR           0x40
#define   TX_DS           0x20

#define   IRQ             IO0PIN&BIT3
#define   BUT             IO0PIN&BIT15

#define   RX_TX_TIME      100000
#define   BUT_TIME        10000


#define   LED_ON          IO0CLR |= BIT10
#define   LED_OFF         IO0SET |= BIT10


