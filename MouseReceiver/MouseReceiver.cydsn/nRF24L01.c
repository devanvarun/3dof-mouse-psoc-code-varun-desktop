#include "nRF24L01.h"
#include "const.h"
#include "NRF_CSN.h"
#include "NRF_CE.h"
#include "SPIM_NRF_Controller.h"
#include "cytypes.h"

#define CSN_TIME      30 //TODO: Check the timing
#define CE_HIGH_TIME  300  //TODO: Check the timing
#define POW_UP_DELAY  6546*6 //~about 1.5ms
uint8 status;

uint8 RX_ADDRESS_P0[5]  = {5,6,7,8,9};//TODO: Modify code so it works with this board
uint8 RX_ADDRESS_P1[5]  = {0,1,2,3,4};//TODO: Modify code so it works with this board
uint8 TX_ADDRESS[5]     = {5,6,7,8,9};//TODO: Modify code so it works with this board
uint8 ADDRESS[5];//TODO: Modify code so it works with this board


void Delay (unsigned long a) { while (--a!=0); }

// Chip Select -> high
void CSN_HIGH  (void)   {  NRF_CSN_Write(0x01);CyDelayUs(1);}

// Chip Select -> low
void CSN_LOW (void)   {  NRF_CSN_Write(0x00);CyDelayUs(1);}


// Chip enable High
void CE_HIGH(void)    { NRF_CE_Write(0x01) ;  CyDelayUs(10); }
// Chip enable low
void CE_LOW(void)     {  NRF_CE_Write(0x00); }


uint8  SPI_SendByte(uint8 spiData) {

//  // Send the data
//  SSP0DR = data;
//
//  // Wait until the character can be sent
//  while (SSP0SR & (1<<4));
//
//  // return received data
//  return SSP0DR;
    uint8 rxStatus = 0x00;
    SPIM_NRF_Controller_WriteTxData(spiData);
    rxStatus = SPIM_NRF_Controller_RX_STATUS_REG;
    while((rxStatus&SPIM_NRF_Controller_STS_RX_FIFO_NOT_EMPTY) != SPIM_NRF_Controller_STS_RX_FIFO_NOT_EMPTY){
        rxStatus = SPIM_NRF_Controller_RX_STATUS_REG;
    }
    return (uint8)SPIM_NRF_Controller_ReadRxData();

}


uint8 SPI_Send_command_with_ADDR (uint8 cmd, uint8 addr, uint8 data_byte)//TODO: Modify code so it works with this board
{
  uint8 temp,command = 0;
  int k;
  int j;
  command = (cmd << 5) | addr;
  CSN_LOW();
  if (cmd == R_REGISTER)
  {
    if (addr == RX_ADDR_P0 || addr == RX_ADDR_P1 || addr == TX_ADDR)
    {

      status=SPI_SendByte(command);
      for (k=0;k!=5;k++)
      {
        ADDRESS[k]=SPI_SendByte(NOP);
      }
      CSN_HIGH();
      return status;
    }
    else
    {
      status=SPI_SendByte(command);
      temp=SPI_SendByte(NOP);
      CSN_HIGH();
      return temp;
    }
  }
  if (cmd == W_REGISTER)
  {
    if (addr == RX_ADDR_P0)
    {
        
       
      status=SPI_SendByte(command);
      for (j=0;j!=5;j++)
        {
          temp=RX_ADDRESS_P0[j];
          SPI_SendByte(temp);
        }
      CSN_HIGH();
      return status;
    }

    if (addr == RX_ADDR_P1)
    {
      status=SPI_SendByte(command);
      for (j=0;j!=5;j++)
        {
          temp=RX_ADDRESS_P1[j];
          SPI_SendByte(temp);
        }
      CSN_HIGH();
      return status;
    }

    if (addr == TX_ADDR)
    {
      status=SPI_SendByte(command);
      for (j=0;j!=5;j++)
        {
          temp=TX_ADDRESS[j];
          SPI_SendByte(temp);
        }
      CSN_HIGH();
      return status;
    }


    else
    {
      temp=SPI_SendByte(command);
      SPI_SendByte(data_byte);
      CSN_HIGH();
      return temp;
    }
  }

  return 1;
}

uint8 SPI_Send_command_without_ADDR (uint8 cmd, uint8 data_byte)//TODO: Modify code so it works with this board
{
  uint8 temp = 0;
  CSN_LOW();
  if (cmd == R_RX_PAYLOAD)
  {
    status=SPI_SendByte(cmd);
    temp=SPI_SendByte(NOP);
    CSN_HIGH();
    return temp;
  }
  if (cmd == W_TX_PAYLOAD)
  {
    status=SPI_SendByte(cmd);
    SPI_SendByte(data_byte);
    CSN_HIGH();
    return status;
  }

    if (cmd == W_TX_PAYLOAD_NOACK)
  {
    status=SPI_SendByte(cmd);
    SPI_SendByte(data_byte);
    CSN_HIGH();
    return status;
  }
  status = SPI_SendByte(cmd);
  CSN_HIGH();
  return status;

}

void nRFSetting(void) {  //TODO: Modify code so it works with this board

  // Discard transmision
  CE_LOW();

  //Write CONFIG register (addres - 0x00)
  //00001010 - CRC enable, power-up, RX
  status = SPI_Send_command_with_ADDR(W_REGISTER, CONFIG_REG_ADDR, 0x0B);
  // read

  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x03, 0x03);

  //Write RX_ADDR_P0 register -> Set receive address data Pipe0 -> address in RX_ADDRESS_P0 array
  status = SPI_Send_command_with_ADDR(W_REGISTER, RX_ADDR_P0, NOP);
  // read
  
  //enable AUTO ACK on data pipe 0
  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x01, 0x01);
  
  //Enable RX on data pipe 0
  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x02, 0x01);
  
  //Setup Address Widths



  //Write TX_ADDR register -> Transmit address. Used for a PTX device only. Address in TX_ADDRESS array
//  status = SPI_Send_command_with_ADDR(W_REGISTER, TX_ADDR, NOP);
  // read
  // status = SPI_Send_command_with_ADDR(R_REGISTER, TX_ADDR, NOP);

  //Write RX_PW_P0 register -> Set number of bytes in RX payload in data pipe0 -> 3 byte
  status = SPI_Send_command_with_ADDR(W_REGISTER, RX_PW_P0, 4);
  // read

  // status = SPI_Send_command_with_ADDR(R_REGISTER, RX_PW_P0, NOP);


 // status = SPI_Send_command_with_ADDR(W_REGISTER, FEATURE_ADDR,0x1);

  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x06, 0x0E);
  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x05, 0x32);

}

void nRF_Test(void){
   CE_LOW();

  //Write CONFIG register (addres - 0x00)
  //00001011 - CRC enable, power-up, TX 
  status = SPI_Send_command_with_ADDR(W_REGISTER, CONFIG_REG_ADDR, 0x02);
  Delay(POW_UP_DELAY);
  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x06, 0x96);
  status = SPI_Send_command_with_ADDR(W_REGISTER, 0x05, 0x32);
   CE_HIGH();
   while(1){}
}