/*******************************************************************************
* File Name: CE_NRF.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "CE_NRF.h"


/*******************************************************************************
* Function Name: CE_NRF_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None 
*  
*******************************************************************************/
void CE_NRF_Write(uint8 value) 
{
    uint8 staticBits = (CE_NRF_DR & (uint8)(~CE_NRF_MASK));
    CE_NRF_DR = staticBits | ((uint8)(value << CE_NRF_SHIFT) & CE_NRF_MASK);
}


/*******************************************************************************
* Function Name: CE_NRF_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void CE_NRF_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(CE_NRF_0, mode);
}


/*******************************************************************************
* Function Name: CE_NRF_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro CE_NRF_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 CE_NRF_Read(void) 
{
    return (CE_NRF_PS & CE_NRF_MASK) >> CE_NRF_SHIFT;
}


/*******************************************************************************
* Function Name: CE_NRF_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 CE_NRF_ReadDataReg(void) 
{
    return (CE_NRF_DR & CE_NRF_MASK) >> CE_NRF_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(CE_NRF_INTSTAT) 

    /*******************************************************************************
    * Function Name: CE_NRF_ClearInterrupt
    ********************************************************************************
    *
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 CE_NRF_ClearInterrupt(void) 
    {
        return (CE_NRF_INTSTAT & CE_NRF_MASK) >> CE_NRF_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */
