/*******************************************************************************
* File Name: Pin_EzI2C.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Pin_EzI2C_H) /* Pins Pin_EzI2C_H */
#define CY_PINS_Pin_EzI2C_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Pin_EzI2C_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    Pin_EzI2C_Write(uint8 value) ;
void    Pin_EzI2C_SetDriveMode(uint8 mode) ;
uint8   Pin_EzI2C_ReadDataReg(void) ;
uint8   Pin_EzI2C_Read(void) ;
uint8   Pin_EzI2C_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Pin_EzI2C_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Pin_EzI2C_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Pin_EzI2C_DM_RES_UP          PIN_DM_RES_UP
#define Pin_EzI2C_DM_RES_DWN         PIN_DM_RES_DWN
#define Pin_EzI2C_DM_OD_LO           PIN_DM_OD_LO
#define Pin_EzI2C_DM_OD_HI           PIN_DM_OD_HI
#define Pin_EzI2C_DM_STRONG          PIN_DM_STRONG
#define Pin_EzI2C_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Pin_EzI2C_MASK               Pin_EzI2C__MASK
#define Pin_EzI2C_SHIFT              Pin_EzI2C__SHIFT
#define Pin_EzI2C_WIDTH              2u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Pin_EzI2C_PS                     (* (reg8 *) Pin_EzI2C__PS)
/* Data Register */
#define Pin_EzI2C_DR                     (* (reg8 *) Pin_EzI2C__DR)
/* Port Number */
#define Pin_EzI2C_PRT_NUM                (* (reg8 *) Pin_EzI2C__PRT) 
/* Connect to Analog Globals */                                                  
#define Pin_EzI2C_AG                     (* (reg8 *) Pin_EzI2C__AG)                       
/* Analog MUX bux enable */
#define Pin_EzI2C_AMUX                   (* (reg8 *) Pin_EzI2C__AMUX) 
/* Bidirectional Enable */                                                        
#define Pin_EzI2C_BIE                    (* (reg8 *) Pin_EzI2C__BIE)
/* Bit-mask for Aliased Register Access */
#define Pin_EzI2C_BIT_MASK               (* (reg8 *) Pin_EzI2C__BIT_MASK)
/* Bypass Enable */
#define Pin_EzI2C_BYP                    (* (reg8 *) Pin_EzI2C__BYP)
/* Port wide control signals */                                                   
#define Pin_EzI2C_CTL                    (* (reg8 *) Pin_EzI2C__CTL)
/* Drive Modes */
#define Pin_EzI2C_DM0                    (* (reg8 *) Pin_EzI2C__DM0) 
#define Pin_EzI2C_DM1                    (* (reg8 *) Pin_EzI2C__DM1)
#define Pin_EzI2C_DM2                    (* (reg8 *) Pin_EzI2C__DM2) 
/* Input Buffer Disable Override */
#define Pin_EzI2C_INP_DIS                (* (reg8 *) Pin_EzI2C__INP_DIS)
/* LCD Common or Segment Drive */
#define Pin_EzI2C_LCD_COM_SEG            (* (reg8 *) Pin_EzI2C__LCD_COM_SEG)
/* Enable Segment LCD */
#define Pin_EzI2C_LCD_EN                 (* (reg8 *) Pin_EzI2C__LCD_EN)
/* Slew Rate Control */
#define Pin_EzI2C_SLW                    (* (reg8 *) Pin_EzI2C__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Pin_EzI2C_PRTDSI__CAPS_SEL       (* (reg8 *) Pin_EzI2C__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Pin_EzI2C_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Pin_EzI2C__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Pin_EzI2C_PRTDSI__OE_SEL0        (* (reg8 *) Pin_EzI2C__PRTDSI__OE_SEL0) 
#define Pin_EzI2C_PRTDSI__OE_SEL1        (* (reg8 *) Pin_EzI2C__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Pin_EzI2C_PRTDSI__OUT_SEL0       (* (reg8 *) Pin_EzI2C__PRTDSI__OUT_SEL0) 
#define Pin_EzI2C_PRTDSI__OUT_SEL1       (* (reg8 *) Pin_EzI2C__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Pin_EzI2C_PRTDSI__SYNC_OUT       (* (reg8 *) Pin_EzI2C__PRTDSI__SYNC_OUT) 


#if defined(Pin_EzI2C__INTSTAT)  /* Interrupt Registers */

    #define Pin_EzI2C_INTSTAT                (* (reg8 *) Pin_EzI2C__INTSTAT)
    #define Pin_EzI2C_SNAP                   (* (reg8 *) Pin_EzI2C__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Pin_EzI2C_H */


/* [] END OF FILE */
