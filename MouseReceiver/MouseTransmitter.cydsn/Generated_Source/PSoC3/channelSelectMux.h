/*******************************************************************************
* File Name: channelSelectMux.h
* Version 1.80
*
*  Description:
*    This file contains the constants and function prototypes for the Analog
*    Multiplexer User Module AMux.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_AMUX_channelSelectMux_H)
#define CY_AMUX_channelSelectMux_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cyfitter_cfg.h"


/***************************************
*        Function Prototypes
***************************************/

void channelSelectMux_Start(void) ;
#define channelSelectMux_Init() channelSelectMux_Start()
void channelSelectMux_FastSelect(uint8 channel) ;
/* The Stop, Select, Connect, Disconnect and DisconnectAll functions are declared elsewhere */
/* void channelSelectMux_Stop(void); */
/* void channelSelectMux_Select(uint8 channel); */
/* void channelSelectMux_Connect(uint8 channel); */
/* void channelSelectMux_Disconnect(uint8 channel); */
/* void channelSelectMux_DisconnectAll(void) */


/***************************************
*         Parameter Constants
***************************************/

#define channelSelectMux_CHANNELS  4u
#define channelSelectMux_MUXTYPE   1
#define channelSelectMux_ATMOSTONE 0

/***************************************
*             API Constants
***************************************/

#define channelSelectMux_NULL_CHANNEL 0xFFu
#define channelSelectMux_MUX_SINGLE   1
#define channelSelectMux_MUX_DIFF     2


/***************************************
*        Conditional Functions
***************************************/

#if channelSelectMux_MUXTYPE == channelSelectMux_MUX_SINGLE
# if !channelSelectMux_ATMOSTONE
#  define channelSelectMux_Connect(channel) channelSelectMux_Set(channel)
# endif
# define channelSelectMux_Disconnect(channel) channelSelectMux_Unset(channel)
#else
# if !channelSelectMux_ATMOSTONE
void channelSelectMux_Connect(uint8 channel) ;
# endif
void channelSelectMux_Disconnect(uint8 channel) ;
#endif

#if channelSelectMux_ATMOSTONE
# define channelSelectMux_Stop() channelSelectMux_DisconnectAll()
# define channelSelectMux_Select(channel) channelSelectMux_FastSelect(channel)
void channelSelectMux_DisconnectAll(void) ;
#else
# define channelSelectMux_Stop() channelSelectMux_Start()
void channelSelectMux_Select(uint8 channel) ;
# define channelSelectMux_DisconnectAll() channelSelectMux_Start()
#endif

#endif /* CY_AMUX_channelSelectMux_H */


/* [] END OF FILE */
