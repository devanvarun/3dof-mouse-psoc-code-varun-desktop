/*******************************************************************************
* File Name: NRF_SCLK.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_NRF_SCLK_H) /* Pins NRF_SCLK_H */
#define CY_PINS_NRF_SCLK_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "NRF_SCLK_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    NRF_SCLK_Write(uint8 value) ;
void    NRF_SCLK_SetDriveMode(uint8 mode) ;
uint8   NRF_SCLK_ReadDataReg(void) ;
uint8   NRF_SCLK_Read(void) ;
uint8   NRF_SCLK_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define NRF_SCLK_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define NRF_SCLK_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define NRF_SCLK_DM_RES_UP          PIN_DM_RES_UP
#define NRF_SCLK_DM_RES_DWN         PIN_DM_RES_DWN
#define NRF_SCLK_DM_OD_LO           PIN_DM_OD_LO
#define NRF_SCLK_DM_OD_HI           PIN_DM_OD_HI
#define NRF_SCLK_DM_STRONG          PIN_DM_STRONG
#define NRF_SCLK_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define NRF_SCLK_MASK               NRF_SCLK__MASK
#define NRF_SCLK_SHIFT              NRF_SCLK__SHIFT
#define NRF_SCLK_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define NRF_SCLK_PS                     (* (reg8 *) NRF_SCLK__PS)
/* Data Register */
#define NRF_SCLK_DR                     (* (reg8 *) NRF_SCLK__DR)
/* Port Number */
#define NRF_SCLK_PRT_NUM                (* (reg8 *) NRF_SCLK__PRT) 
/* Connect to Analog Globals */                                                  
#define NRF_SCLK_AG                     (* (reg8 *) NRF_SCLK__AG)                       
/* Analog MUX bux enable */
#define NRF_SCLK_AMUX                   (* (reg8 *) NRF_SCLK__AMUX) 
/* Bidirectional Enable */                                                        
#define NRF_SCLK_BIE                    (* (reg8 *) NRF_SCLK__BIE)
/* Bit-mask for Aliased Register Access */
#define NRF_SCLK_BIT_MASK               (* (reg8 *) NRF_SCLK__BIT_MASK)
/* Bypass Enable */
#define NRF_SCLK_BYP                    (* (reg8 *) NRF_SCLK__BYP)
/* Port wide control signals */                                                   
#define NRF_SCLK_CTL                    (* (reg8 *) NRF_SCLK__CTL)
/* Drive Modes */
#define NRF_SCLK_DM0                    (* (reg8 *) NRF_SCLK__DM0) 
#define NRF_SCLK_DM1                    (* (reg8 *) NRF_SCLK__DM1)
#define NRF_SCLK_DM2                    (* (reg8 *) NRF_SCLK__DM2) 
/* Input Buffer Disable Override */
#define NRF_SCLK_INP_DIS                (* (reg8 *) NRF_SCLK__INP_DIS)
/* LCD Common or Segment Drive */
#define NRF_SCLK_LCD_COM_SEG            (* (reg8 *) NRF_SCLK__LCD_COM_SEG)
/* Enable Segment LCD */
#define NRF_SCLK_LCD_EN                 (* (reg8 *) NRF_SCLK__LCD_EN)
/* Slew Rate Control */
#define NRF_SCLK_SLW                    (* (reg8 *) NRF_SCLK__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define NRF_SCLK_PRTDSI__CAPS_SEL       (* (reg8 *) NRF_SCLK__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define NRF_SCLK_PRTDSI__DBL_SYNC_IN    (* (reg8 *) NRF_SCLK__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define NRF_SCLK_PRTDSI__OE_SEL0        (* (reg8 *) NRF_SCLK__PRTDSI__OE_SEL0) 
#define NRF_SCLK_PRTDSI__OE_SEL1        (* (reg8 *) NRF_SCLK__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define NRF_SCLK_PRTDSI__OUT_SEL0       (* (reg8 *) NRF_SCLK__PRTDSI__OUT_SEL0) 
#define NRF_SCLK_PRTDSI__OUT_SEL1       (* (reg8 *) NRF_SCLK__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define NRF_SCLK_PRTDSI__SYNC_OUT       (* (reg8 *) NRF_SCLK__PRTDSI__SYNC_OUT) 


#if defined(NRF_SCLK__INTSTAT)  /* Interrupt Registers */

    #define NRF_SCLK_INTSTAT                (* (reg8 *) NRF_SCLK__INTSTAT)
    #define NRF_SCLK_SNAP                   (* (reg8 *) NRF_SCLK__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins NRF_SCLK_H */


/* [] END OF FILE */
