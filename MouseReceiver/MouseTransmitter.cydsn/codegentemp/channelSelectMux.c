/*******************************************************************************
* File Name: channelSelectMux.c
* Version 1.80
*
*  Description:
*    This file contains all functions required for the analog multiplexer
*    AMux User Module.
*
*   Note:
*
*******************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "channelSelectMux.h"

static uint8 channelSelectMux_lastChannel = channelSelectMux_NULL_CHANNEL;


/*******************************************************************************
* Function Name: channelSelectMux_Start
********************************************************************************
* Summary:
*  Disconnect all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_Start(void) 
{
    uint8 chan;

    for(chan = 0u; chan < channelSelectMux_CHANNELS ; chan++)
    {
#if (channelSelectMux_MUXTYPE == channelSelectMux_MUX_SINGLE)
        channelSelectMux_Unset(chan);
#else
        channelSelectMux_CYAMUXSIDE_A_Unset(chan);
        channelSelectMux_CYAMUXSIDE_B_Unset(chan);
#endif
    }

    channelSelectMux_lastChannel = channelSelectMux_NULL_CHANNEL;
}


#if (!channelSelectMux_ATMOSTONE)
/*******************************************************************************
* Function Name: channelSelectMux_Select
********************************************************************************
* Summary:
*  This functions first disconnects all channels then connects the given
*  channel.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_Select(uint8 channel) 
{
    channelSelectMux_DisconnectAll();        /* Disconnect all previous connections */
    channelSelectMux_Connect(channel);       /* Make the given selection */
    channelSelectMux_lastChannel = channel;  /* Update last channel */
}
#endif


/*******************************************************************************
* Function Name: channelSelectMux_FastSelect
********************************************************************************
* Summary:
*  This function first disconnects the last connection made with FastSelect or
*  Select, then connects the given channel. The FastSelect function is similar
*  to the Select function, except it is faster since it only disconnects the
*  last channel selected rather than all channels.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_FastSelect(uint8 channel) 
{
    /* Disconnect the last valid channel */
    if( channelSelectMux_lastChannel != channelSelectMux_NULL_CHANNEL)
    {
        channelSelectMux_Disconnect(channelSelectMux_lastChannel);
    }

    /* Make the new channel connection */
#if (channelSelectMux_MUXTYPE == channelSelectMux_MUX_SINGLE)
    channelSelectMux_Set(channel);
#else
    channelSelectMux_CYAMUXSIDE_A_Set(channel);
    channelSelectMux_CYAMUXSIDE_B_Set(channel);
#endif


    channelSelectMux_lastChannel = channel;   /* Update last channel */
}


#if (channelSelectMux_MUXTYPE == channelSelectMux_MUX_DIFF)
#if (!channelSelectMux_ATMOSTONE)
/*******************************************************************************
* Function Name: channelSelectMux_Connect
********************************************************************************
* Summary:
*  This function connects the given channel without affecting other connections.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_Connect(uint8 channel) 
{
    channelSelectMux_CYAMUXSIDE_A_Set(channel);
    channelSelectMux_CYAMUXSIDE_B_Set(channel);
}
#endif

/*******************************************************************************
* Function Name: channelSelectMux_Disconnect
********************************************************************************
* Summary:
*  This function disconnects the given channel from the common or output
*  terminal without affecting other connections.
*
* Parameters:
*  channel:  The channel to disconnect from the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_Disconnect(uint8 channel) 
{
    channelSelectMux_CYAMUXSIDE_A_Unset(channel);
    channelSelectMux_CYAMUXSIDE_B_Unset(channel);
}
#endif

#if (channelSelectMux_ATMOSTONE)
/*******************************************************************************
* Function Name: channelSelectMux_DisconnectAll
********************************************************************************
* Summary:
*  This function disconnects all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void channelSelectMux_DisconnectAll(void) 
{
    if(channelSelectMux_lastChannel != channelSelectMux_NULL_CHANNEL) 
    {
        channelSelectMux_Disconnect(channelSelectMux_lastChannel);
        channelSelectMux_lastChannel = channelSelectMux_NULL_CHANNEL;
    }
}
#endif

/* [] END OF FILE */
