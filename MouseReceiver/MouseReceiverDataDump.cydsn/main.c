/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include "nRF24L01.h"
#include "const.h"

#define ERROR_NO_ERROR  0
uint8 dataReceivedFlag = 0;
uint8 rxCount = 0;
int8 Mouse_Data[3] = {0, 0, 0}; /* [0] = Buttons, [1] = X-Axis, [2] = Y-Axis */


int Rx_PL_Read( uint8 *Data, int Data_Len);

int main()
{

    uint8 status;
    uint8 Rx_Data[32];
    int i;
    
    PWM_1_Start();
    
    isr_nrf_Start();
    CyGlobalIntEnable; 
    
    LCD_Start();

    SPIM_NRF_Controller_Start();
    nRFSetting();
    CE_HIGH(); 
    //nRF_Test();
    LCD_Position(0,0);
    LCD_PrintNumber(status);
    USBFS_1_Start(0, USBFS_1_DWR_VDDD_OPERATION);   /* Start USBFS Operation/device 0 and with 5V operation */ 	
//	while(!USBFS_1_bGetConfiguration());      		/* Wait for Device to enumerate */
    USBFS_1_LoadInEP(1, (uint8 *)Mouse_Data, 3); 	/* Loads an inital value into EP1 and sends it out to the PC */
    
    for(;;)
    {
        /* Place your application code here. */
        
            if(dataReceivedFlag == 1){
                dataReceivedFlag = 0;   
                status = SPI_Send_command_without_ADDR(NOP,NOP);
                if ((status  & RX_DR) == RX_DR )
                {
                LCD_Position(0,0);
                LCD_PrintString("Data Rcvd");
                SPI_Send_command_with_ADDR(W_REGISTER, STATUS_ADDR, (status| RX_DR));
                CE_LOW();
                //SPI_Send_command_without_ADDR(CMD_RX_PL_WID,NOP);
                //rxCount = SPI_Send_command_without_ADDR(NOP,NOP);
                Rx_PL_Read(&Rx_Data[0],32);  
             //   LCD_Position(1,0);
                for(i=0;i<3;i++){   
             //       LCD_PrintNumber(Rx_Data[i]);
                    Mouse_Data[i] = Rx_Data[i];
                  }
                
                USBFS_1_LoadInEP(1, (uint8 *)Mouse_Data, 3); 	/* Load latest mouse data into EP1 and send to PC */
                while(!USBFS_1_bGetEPAckState(1));  			/* Wait for ACK before loading data */
		       
                isr_nrf_ClearPending();
                CE_HIGH();
                } 
            }       
                
    }
}

int Rx_PL_Read( uint8 *Data, int Data_Len){
  int j;
  CSN_LOW();
  SPI_SendByte(R_RX_PAYLOAD);
  for (j=0;j<(Data_Len);j++){
    *(Data+j) = SPI_SendByte(NOP);
  }
  CSN_HIGH();
  return ERROR_NO_ERROR;
}

 
/* [] END OF FILE */
