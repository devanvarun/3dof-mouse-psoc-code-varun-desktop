/*******************************************************************************
 * File Name: project.h
 * PSoC Creator 3.0
 *
 *  Description:
 *  This file is automatically generated by PSoC Creator and should not 
 *  be edited by hand.
 *
 *
 ********************************************************************************
 * Copyright 2008-2013, Cypress Semiconductor Corporation.  All rights reserved.
 * You may use this file only in accordance with the license, terms, conditions, 
 * disclaimers, and limitations in the end user license agreement accompanying 
 * the software package with which this file was provided.
 ********************************************************************************/

#include <cyfitter_cfg.h>
#include <cydevice.h>
#include <cydevice_trm.h>
#include <cyfitter.h>
#include <cydisabledsheets.h>
#include <NRF_MISO.h>
#include <NRF_MISO_aliases.h>
#include <NRF_MOSI.h>
#include <NRF_MOSI_aliases.h>
#include <NRF_SCLK.h>
#include <NRF_SCLK_aliases.h>
#include <SPIM_NRF_Controller.h>
#include <SPIM_NRF_Controller_PVT.h>
#include <NRF_CSN.h>
#include <NRF_CSN_aliases.h>
#include <NRF_CE.h>
#include <NRF_CE_aliases.h>
#include <Pin_1.h>
#include <Pin_1_aliases.h>
#include <PWM_1.h>
#include <Clock_1.h>
#include <LCD.h>
#include <NRF_IRQ.h>
#include <NRF_IRQ_aliases.h>
#include <isr_nrf.h>
#include <USBFS_1.h>
#include <USBFS_1_audio.h>
#include <USBFS_1_cdc.h>
#include <USBFS_1_hid.h>
#include <USBFS_1_midi.h>
#include <USBFS_1_pvt.h>
#include <SPIM_NRF_Controller_IntClock.h>
#include <LCD_LCDPort.h>
#include <LCD_LCDPort_aliases.h>
#include <USBFS_1_Dm.h>
#include <USBFS_1_Dm_aliases.h>
#include <USBFS_1_Dp.h>
#include <USBFS_1_Dp_aliases.h>
#include <CyDmac.h>
#include <CyFlash.h>
#include <CyLib.h>
#include <cypins.h>
#include <cyPm.h>
#include <CySpc.h>
#include <cytypes.h>
#include <PSoC3_8051.h>

/*[]*/

