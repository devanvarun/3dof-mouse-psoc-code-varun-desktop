/*******************************************************************************
* File Name: CE_NRF.h  
* Version 1.90
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CE_NRF_H) /* Pins CE_NRF_H */
#define CY_PINS_CE_NRF_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "CE_NRF_aliases.h"


/***************************************
*        Function Prototypes             
***************************************/    

void    CE_NRF_Write(uint8 value) ;
void    CE_NRF_SetDriveMode(uint8 mode) ;
uint8   CE_NRF_ReadDataReg(void) ;
uint8   CE_NRF_Read(void) ;
uint8   CE_NRF_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define CE_NRF_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define CE_NRF_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define CE_NRF_DM_RES_UP          PIN_DM_RES_UP
#define CE_NRF_DM_RES_DWN         PIN_DM_RES_DWN
#define CE_NRF_DM_OD_LO           PIN_DM_OD_LO
#define CE_NRF_DM_OD_HI           PIN_DM_OD_HI
#define CE_NRF_DM_STRONG          PIN_DM_STRONG
#define CE_NRF_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define CE_NRF_MASK               CE_NRF__MASK
#define CE_NRF_SHIFT              CE_NRF__SHIFT
#define CE_NRF_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define CE_NRF_PS                     (* (reg8 *) CE_NRF__PS)
/* Data Register */
#define CE_NRF_DR                     (* (reg8 *) CE_NRF__DR)
/* Port Number */
#define CE_NRF_PRT_NUM                (* (reg8 *) CE_NRF__PRT) 
/* Connect to Analog Globals */                                                  
#define CE_NRF_AG                     (* (reg8 *) CE_NRF__AG)                       
/* Analog MUX bux enable */
#define CE_NRF_AMUX                   (* (reg8 *) CE_NRF__AMUX) 
/* Bidirectional Enable */                                                        
#define CE_NRF_BIE                    (* (reg8 *) CE_NRF__BIE)
/* Bit-mask for Aliased Register Access */
#define CE_NRF_BIT_MASK               (* (reg8 *) CE_NRF__BIT_MASK)
/* Bypass Enable */
#define CE_NRF_BYP                    (* (reg8 *) CE_NRF__BYP)
/* Port wide control signals */                                                   
#define CE_NRF_CTL                    (* (reg8 *) CE_NRF__CTL)
/* Drive Modes */
#define CE_NRF_DM0                    (* (reg8 *) CE_NRF__DM0) 
#define CE_NRF_DM1                    (* (reg8 *) CE_NRF__DM1)
#define CE_NRF_DM2                    (* (reg8 *) CE_NRF__DM2) 
/* Input Buffer Disable Override */
#define CE_NRF_INP_DIS                (* (reg8 *) CE_NRF__INP_DIS)
/* LCD Common or Segment Drive */
#define CE_NRF_LCD_COM_SEG            (* (reg8 *) CE_NRF__LCD_COM_SEG)
/* Enable Segment LCD */
#define CE_NRF_LCD_EN                 (* (reg8 *) CE_NRF__LCD_EN)
/* Slew Rate Control */
#define CE_NRF_SLW                    (* (reg8 *) CE_NRF__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define CE_NRF_PRTDSI__CAPS_SEL       (* (reg8 *) CE_NRF__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define CE_NRF_PRTDSI__DBL_SYNC_IN    (* (reg8 *) CE_NRF__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define CE_NRF_PRTDSI__OE_SEL0        (* (reg8 *) CE_NRF__PRTDSI__OE_SEL0) 
#define CE_NRF_PRTDSI__OE_SEL1        (* (reg8 *) CE_NRF__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define CE_NRF_PRTDSI__OUT_SEL0       (* (reg8 *) CE_NRF__PRTDSI__OUT_SEL0) 
#define CE_NRF_PRTDSI__OUT_SEL1       (* (reg8 *) CE_NRF__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define CE_NRF_PRTDSI__SYNC_OUT       (* (reg8 *) CE_NRF__PRTDSI__SYNC_OUT) 


#if defined(CE_NRF__INTSTAT)  /* Interrupt Registers */

    #define CE_NRF_INTSTAT                (* (reg8 *) CE_NRF__INTSTAT)
    #define CE_NRF_SNAP                   (* (reg8 *) CE_NRF__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins CE_NRF_H */


/* [] END OF FILE */
